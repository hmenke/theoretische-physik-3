% Henri Menke, Michael Schmid, Marcel Klett, Jan Schnabel 2013 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

% Vorlesung am 28.06.2013
% TeX: Henri 

\renewcommand{\printfile}{2013-06-28}

Damit erhalten wir
%
\begin{align*}
	x' &= \gamma(v) x - v \gamma(v) t = \gamma(v) (x-vt) \\
	\left. \frac{\mathrm{d}x}{\mathrm{d}t} \right|_{x' = \mathrm{const}} &= v = - \frac{b(v)}{\gamma(v)}
\end{align*}
%
folglich
%
\begin{align*}
	v \to 0 \implies \Lambda = \mathrm{id} \implies \gamma(v \to 0) = 1
\end{align*}
%
Dies spiegelt die Stetigkeit bei $v \to 0$ wider.
%
\begin{align*}
	\Sigma' \xrightarrow{\Lambda^{-1}} \Sigma
\end{align*}
%
$\Lambda^{-1}$ soll aus $\Lambda$ durch $v \to - v$ hervorgehen.
%
\begin{align*}
	x &= \gamma(-v) (x' + v t')
\intertext{Bilde nun die Ableitung von $x'$ nach $t'$}
	\left. \frac{\mathrm{d}x'}{\mathrm{d}t'} \right|_{x = \mathrm{const}} &= - v
\intertext{Dies erfüllt offensichtlich \eqref{eq:2013-06-26-1}}
	x &= \gamma(v) \gamma(-v) x - \gamma(v) \dot{\gamma}(-v) v t + \gamma(-v) v t' \\
	t' &= \gamma(v) \left[ t - \frac{\gamma(v)\gamma(-v) - 1}{\gamma(v) \gamma(-v)} \frac{x}{v} \right] \\
	x' &= \gamma(v) [ x - v t ]
\end{align*}
%
$\gamma(v)$ ist dimensionslos; wenn es von $v$ abhängt, muss es zusätzlich parametrisch von einer weiteren, festen Geschwindigkeit $V_0$ abhängen:
$\gamma = \gamma(v/V_0)$. Im Limes $v/V_0 \to 0$ soll die Galileitransformation wieder entstehen: $\gamma(v/V_0) = 1 + \mathcal{O}\left( ( v/V_0 )^2 \right)$

\begin{notice}[Annahme:]
	$\gamma$ ist überhaupt vom Vorzeichen von $v$ unabhängig.
	%
	\begin{align*}
		t' &= \gamma \left[ t - \frac{\gamma^2 - 1}{\gamma^2} \frac{x}{v} \right] \\
		x' &= \gamma [ x - v t ]
	\end{align*}
\end{notice}

\begin{theorem}[Definition]
	\begin{align*}
		\frac{\gamma^2 (v/V_0) - 1}{\gamma^2 (v/V_0)} &\eqcolon \frac{v^2}{V_0^2 f\left( \frac{v}{V_0} \right)} \\
		V_0^2 f\left( \frac{v}{V_0} \right) &\eqcolon V^2 \; , \quad V^2 > 0 \iff \gamma > 1
	\end{align*}
\end{theorem}

Damit ergibt sich für die Standardkonfiguration
%
\begin{equation*}
	\boxed{
		\begin{aligned}
			x' &= \gamma [ x - v t ] \\
			y' &= y \\
			z' &= z \\
			t' &= \gamma \left[ t - \frac{v}{V^2} x \right]
		\end{aligned}
	}
	\; \text{mit} \; \gamma = \frac{1}{\sqrt{1 - (v/V)^2}}
\end{equation*}

Nun werden die Eigenschaften von $V^2 = V_0^2 f(v/V_0)$ aus den Gruppeneigenschaften von $\Lambda$ bestimmt.

\begin{tikzcd}
	(x,t) \arrow{r}{v} \arrow[bend right]{rr}[swap]{v''(v,v')}
	& (x',t') \arrow{r}{v'}
	& (x'',t'')
\end{tikzcd}
%
\begin{align*}
	x'' &= \gamma'(x'-v't') \\
	t'' &= \gamma'\left( t - \frac{v'}{{V'}^2} x' \right)
\end{align*}
%
wobei
%
\begin{align*}
	\gamma' &= \left[ 1 - \left( \frac{v'}{V'} \right)^2 \right]^{-1/2} \\
	{v'}^2 &= V_0^2 f\left( \frac{v'}{V_0} \right)
\end{align*}
%
Jetzt Einsetzen von $x'$ und $t'$:
%
\begin{align}
	x''
	&= \gamma \left[
		\gamma(x-vt) - v' \gamma\left( t - \frac{v}{V^2} x \right)
	\right]
	= \gamma \gamma' \left[ 
		\left( 1 + \frac{vv'}{V^2} \right) x - (v + v') t
	\right] \notag \\
	x''
	&= \gamma \gamma' \left( 1 + \frac{vv'}{V^2} \right) \left[ x - \frac{v + v'}{1 + \frac{v v'}{V^2}} t \right]
	\overset{!}{{}={}} \gamma'' [ x - v t ] \label{eq:2013-06-28-2}
\end{align}

Nun wollen wir $t''$ bestimmen.
%
\begin{align}
	t''
	&= \gamma' \left[ \gamma \left( t - \frac{v}{V^2} x \right) - \frac{v'}{ {V'}^2 } \gamma(x-vt) \right] \notag \\
	&= \gamma \gamma' \left[ \left( 1 + \frac{vv'}{ {V'}^2 } \right) t - \left( \frac{v}{V^2} + \frac{v'}{ {V'}^2 } \right) \right] \notag \\
	t''
	&= \gamma \gamma' \left( 1 + \frac{vv'}{ {V'}^2 } \right) \left[ t - \frac{\frac{v}{V^2} + \frac{v'}{ {V'}^2 }}{1 + \frac{vv'}{ {V'}^2 }} x \right]
	\overset{!}{{}={}} \gamma'' \left[ t - \frac{v''}{ {V''}^2 } x \right] \label{eq:2013-06-28-3}
\end{align}
%
wobei
%
\begin{align*}
	\gamma'' &= \left[ 1 - \frac{ {v''}^2 }{V_0^2 f\left( \frac{v''}{V_0} \right)} \right]^{-1/2}
\end{align*}

\eqref{eq:2013-06-28-2} und \eqref{eq:2013-06-28-3} gelten für alle $x,t$. Speziell:
%
\begin{align}
	\eqref{eq:2013-06-28-2}|_{t=0} \implies \gamma \gamma' \left( 1 + \frac{vv'}{V^2} \right) &= \gamma'' \label{eq:2013-06-28-4} \\
	\eqref{eq:2013-06-28-3}|_{x=0} \implies \gamma \gamma' \left( 1 + \frac{vv'}{ {V'}^2} \right) &= \gamma'' \label{eq:2013-06-28-5} \\
	\implies V^2 = {V'}^2 \notag
\end{align}

Mit \eqref{eq:2013-06-28-2} und \eqref{eq:2013-06-28-4} gilt
%
\begin{gather*}
	v'' = \frac{v + v'}{1 + \frac{vv'}{V^2}}
\end{gather*}
%
Dies ist das \acct{Additionstheorem der Geschwindigkeiten} (hier speziell: $\bm{v} \parallel \bm{v}' \parallel \bm{v}''$).
%
\begin{align*}
	\text{\eqref{eq:2013-06-28-3} und \eqref{eq:2013-06-28-5}}
	&\implies \frac{\frac{v}{V^2} + \frac{v'}{ {V'}^2 }}{1 + \frac{vv'}{ {V'}^2 }} = \frac{v''}{ {V''}^2 } = \frac{\frac{v}{ {V''}^2 } + \frac{v'}{ {V''}^2 }}{1 + \frac{vv'}{V^2}} \\
	\overset{V^2 = {V'}^2}{\implies} V^2 = {V'}^2 = {V''}^2
\end{align*}
%
d.h.
%
\begin{align*}
	f\left( \frac{v}{V_0} \right) = f\left( \frac{v'}{V_0} \right) = f\left( \frac{v''}{V_0} \right) \implies f = \mathrm{const}
\end{align*}
%
Wähle nun o.B.d.A.\ $f = 1$. Daraus folgt, dass $V^2 = V_0^2$ eine invariante Geschwindigkeit ist.

Zur Bedeutung von $V_0$:
%
\begin{enumerate}
	\item 
		\[
			\begin{aligned}
				V_0^2 &= \infty \implies \Lambda : \text{Galileitranformation} \\
				V_0^2 &= 0 \implies \gamma = \mathrm{i} \cdot 0 : \text{unphysikalisch, da $x',t' \in \mathbb{R}$}
			\end{aligned}
		\]
	\item
		\[
			\begin{aligned}
				t' = \gamma \left( t - \frac{v}{V_0^2} x \right)
			\end{aligned}
		\]
		%
		speziell $x=0 \implies t' = \gamma t$
		%
		\begin{enumerate}
			\item Wäre $\gamma < 1$, so würde $\Lambda$ von der Gegenwart $t$ in $\Sigma$ in die Vergangenheit $t' < t$ in $\Sigma'$ transformieren. Wegen $(\gamma^2 - 1)/\gamma^2 = v^2/V^2 = v^2/V_0^2$ bedeutet dies, dass $V_0^2 > 0$ sicherstellt, dass Zukunft und Vergangenheit nicht vertauscht werden kann.

			\item Wäre $V_0^2 < 0$, sowie $v'' = \infty$ für $v v' = - V_0^2$ (siehe Additionstheorem) unphysikalisch.
		\end{enumerate}
	
	\item Seien $|\frac{v}{V_0}| < 1$ und $|\frac{v'}{V_0}| < 1$.
		%
		\begin{figure}
			\centering
			\begin{pspicture}(-2,-1.5)(2,1.5)
				\psaxes[labels=none,ticks=none,arrows=->](0,0)(-2,-1.5)(2,1.5)[{\color{DimGray} $\zeta$}, -90][{\color{DimGray} $\tanh\zeta$}, 180]
				\psyTick(1){\color{DimGray} 1}
				\psyTick(-1){\color{DimGray} -1}
				\psplot[linecolor=MidnightBlue]{-2}{2}{TANH(x)}
			\end{pspicture}
			\caption{Tangens hyperbolicus}
		\end{figure}
		%
		\begin{align*}
			\exists \zeta,\zeta' \in \mathbb{R} : \frac{v}{V_0} \eqcolon \tanh \zeta , \frac{v'}{V_0} = \tanh \zeta'
		\end{align*}
		%
		\begin{align*}
			&\implies \frac{\tanh \zeta + \tanh \zeta'}{1 + (\tanh \zeta)(\tanh \zeta')} = \tanh (\zeta + \zeta') \\
			&\implies \left|\frac{v''}{V_0}\right| < 1
		\end{align*}
		%
		Durch Addition von Geschwindigkeiten kann $V_0$ nicht überschritten werden. Das bedeutet: $V_0$ ist eine Grenzgeschwindigkeit.
		%
		\begin{align*}
			V_0'' = \frac{V_0 + V_0'}{1 + \frac{V_0 V_0'}{V_0'}} \overset{V_0 = V_0'}{{}={}} \frac{2 V_0}{2} = V_0
		\end{align*}
		%
		Die Grenzgeschwindigkeit bleibt unter $\Lambda$ invariant.

	\item $V_0 ={} ?$: Physikalisches Problem. $V_0$ muss groß gegenüber >>normalen<< Geschwindigkeiten sein, da die Galilei-Kovarianz der Mechanik eine gute Näherung darstellt.
\end{enumerate}

Michelson-Morley Experiment: Lichtgeschwindigkeit ist unabhängig vom Bewegungszustand der Lichtquelle.

Einstein postuliert daher: Lichtkegel bleibt unter $\Lambda$ invariant.
%
\begin{figure}
	\centering
	\begin{pspicture}(-0.3,-0.5)(4,4)
		\psaxes[labels=none,ticks=none,arrows=->](0,0)(-0.3,-0.3)(4.4,4.4)[{\color{DimGray} $\bm{r}$},-90][{\color{DimGray} $ct$},180]
		\psxTick(2){\color{DimGray} \bm{r}_0}
		\psyTick(2){\color{DimGray} t_0}
		\rput(2,2){
			\psline[linecolor=MidnightBlue](-2;45)(2;45)
			\psline[linecolor=MidnightBlue](-2;135)(2;135)
			\psellipse[linecolor=MidnightBlue](0,1)(1,0.25)
			\psellipse[linecolor=MidnightBlue](0,-1)(1,0.25)
			\psdot*[linecolor=DarkOrange3](0,0)
		}
	\end{pspicture}
	\caption{Lichtkegel $= \{ \{\bm{r},t\} \in \mathbb{R}^3 \times \mathbb{R} | |\bm{r}-\bm{r}_0| = c |t-t_0| \}$}
\end{figure}
%
Diese Punktmenge des Lichtkegels soll unter $\Lambda$ auf sich selbst abgebildet werden.
%
\begin{align*}
	\Lambda :
	t \to t' &, \bm{r} \to \bm{r}' \\
	t_0 \to t_0' &, \bm{r}_0 \to \bm{r}_0'
\end{align*}
%
so, dass $|\bm{r}' - \bm{r}_0'| = c |t-t_0'|$, d.h.
%
\begin{align*}
	c^2 (t-t_0)^2 - (\bm{r} - \bm{r}_0)^2 = 0 \iff c^2 (t'-t_0')^2 - (\bm{r}' - \bm{r}_0')^2 = 0
\end{align*}
%
Dies wird von $\Lambda$ geleistet, genau dann, wenn $V_0 = c$ mit der Vakuumlichtgeschwindigkeit $c$.

Allgemein: Invarianz von $c^2 (t-t_0)^2 - (\bm{r} - \bm{r}_0)^2$, d.h.\ es soll gelten
%
\begin{gather*}
	c^2 (t-t_0)^2 - (\bm{r} - \bm{r}_0)^2 = c^2 (t'-t_0')^2 - (\bm{r}' - \bm{r}_0')^2
\\ 
	\begin{aligned}
		\iff &c^2 \gamma^2 \left[ t - \frac{v}{c^2} x - t_0 + \frac{v}{c^2} x_0 \right]^2 - \gamma^2 \left[ x - x_0 - v (t-t_0) \right]^2
	\\
		&\quad - (y-y_0)^2 - (z-z_0)^2 = c^2 (t-t_0)^2 - (x-x_0)^2 - (y-y_0)^2 - (z-z_0)^2
	\end{aligned}
\\
	c^2 \gamma^2  \left[ (t-t_0) - \frac{v}{c^2} (x-x_0) \right]^2 - \gamma^2 \left[ (x-x_0) - v (t-t_0) \right]^2 = c^2 (t-t_0)^2 - (x-x_0)^2
\\
	\begin{aligned}
		\iff &c^2 \gamma^2 (t-t_0)^2 - 2 v \gamma^2 (t-t_0) (x-x_0) + \frac{v^2}{c^2} \gamma^2 (x-x_0)^2 - \gamma^2 (x-x_0)^2
	\\
		&\quad + 2 v \gamma^2 (x-x_0) (t-t_0) - \gamma^2 v^2 (t-t_0)^2 - c^2 (t-t_0)^2 + (x-x_0)^2 = 0
	\end{aligned}
\\
	c^2 (t-t_0)^2 \left\{ \gamma^2 - \gamma^2 \frac{v^2}{c^2} - 1 \right\} - (x-x_0) \left\{ - \frac{v^2}{c^2} \gamma^2 + \gamma^2 - 1 \right\} = 0
\\
	\left\{ \gamma^2 \left( 1 - \frac{v^2}{c^2} \right) - 1 \right\} \underbrace{\left\{ c^2 (t-t_0)^2 - (x-x_0)^2 \right\}}_{\text{beliebig}} = 0
\\
	\iff \gamma^2 = \frac{1}{1-\frac{v^2}{V_0^2}} = \frac{1}{1-\frac{v^2}{c^2}} \implies V_0 = c
\end{gather*}

\begin{notice}[Zusammenfassung:]
	Haben wir eine Lorentztransformation von einem Inertialsystem (IS) in ein gestrichenes Inertialsystem (IS') in Stanardkonfiguration ($\Lambda_\mathrm{SK}$)
	%
	\begin{equation*}
		\boxed{
			\begin{aligned}
				x' &= \gamma [ x - v t ] \\
				y' &= y \\
				z' &= z \\
				t' &= \gamma \left[ t - \frac{v}{c^2} x \right]
			\end{aligned}
		}
		\; \text{mit} \; \gamma = \frac{1}{\sqrt{1 - (v/c)^2}}
	\end{equation*}
	%
	Die allgemeine Poincar\'e-Transformation IS $\to$ IS' setzt sich aus $\Lambda_\mathrm{SK}$, Raum-Zeit-Translation, Raum-Zeit Spiegelung und Raumdrehungen zusammen.
	%
	\begin{align*}
		\frac{v}{c} \ll 1 : \Lambda \to \Lambda_\mathrm{Galilei}
	\end{align*}
\end{notice}

