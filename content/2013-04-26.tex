% Henri Menke, Michael Schmid, Marcel Klett, Jan Schnabel 2013 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

% Vorlesung am 10.04.2013
% TeX: Jan

\renewcommand{\printfile}{2013-04-26}

\section{Feldgleichungen in differentieller Form}

Mehr zu Differentialoperatoren und deren Darstellungen findet man auch in \cite[(A-3) S.\ 107]{cartarius:mathematische_methoden}.

\subsection{Gradient, \texorpdfstring{$\nabla$}{∇}- Operator}

Sei ein skalares Feld $\phi(\bm{r})$ gegeben. Die Orte an denen $\phi(\bm{r})= \mathrm{const}$, bzw. $\phi(x,y,z)= \mathrm{const}$ gilt, definieren eine Fläche. Entlang einer Kurve $\mathcal{C}$ gilt es eine Parametrisierung dieser einzuführen, was mathematisch wie folgt zum Ausdruck gebracht wird:
%
\begin{align*}
	\phi|_\mathcal{C} &= \phi(\bm{r}(s)) = \phi\left(x(s),y(s),z(s)\right).
\end{align*}
%
Dabei ist der Tangentenvektor durch $\frac{\mathrm{d}}{\mathrm{d}s}\bm{r}(s)$ gegeben. Betrachten wir die Änderung von $\phi$ entlang der Kurve $\mathcal{C}$, so ergibt sich diese aus:
%
\begin{align*}
	\frac{\mathrm{d}}{\mathrm{d}s}\phi(\bm{r}(s))\cdot\Delta s &= \Delta\phi\\
\intertext{beziehungsweise:}
	\frac{\mathrm{d}}{\mathrm{d}s}\phi(\bm{r}(s))
	&=
	\left.\frac{\partial\phi}{\partial x} \right|_{\mathcal{C}}
	\frac{\mathrm{d}x}{\mathrm{d}s}
	+ \left. \frac{\partial\phi}{\partial y} \right|_{\mathcal{C}}
	\frac{\mathrm{d}y}{\mathrm{d}s}
	+ \left. \frac{\partial\phi}{\partial z} \right|_{\mathcal{C}}
	\frac{\mathrm{d}z}{\mathrm{d}s} \\
	&= \nabla\phi(\bm{r}(s)) \cdot \frac{\mathrm{d}}{\mathrm{d}s} \bm{r}(s)
\end{align*}
%
Daraus ergibt sich auch die koordinatenunabhängige \acct*{Definition des Gradienten}\index{Differentialoperator!Gradient}, gemäß:
%
\begin{align*}
	\mathrm{d}\phi &= (\nabla\phi)\cdot\mathrm{d}\bm{r} = (\grad\phi)\cdot\mathrm{d}\bm{r}
\end{align*}
%
Der dabei verwendete \acct{Nabla-Operator} ($\nabla$) ist (für Physiker) sowohl ein Vektor als auch ein Differentialoperator. In kartesischen Koordinaten ausgeschrieben lautet er:
%
\begin{align*}
	\nabla &= \bm{e}_x\partial_x + \bm{e}_y\partial_y + \bm{e}_z\partial_z
\end{align*}
%
Der Gradient selbst ist eine Abbildung vom Funktionenraum in den $\mathbb{R}^3$, das heißt \[ \grad : \text{skalares Feld} \to \text{Vektorfeld}. \] Ein Spezialfall ergibt sich, wenn eine Kurve $\tilde{\mathcal{C}}$ in der Fläche mit $\phi(\bm{r})= \mathrm{const}$ liegt. Dann gilt nämlich:
%
\begin{align*}
	\frac{\mathrm{d}\phi(\bm{r}(s))}{\mathrm{d}s} &= 0 = \Bigl(\nabla\phi(\bm{r}(s))\Bigr)\cdot\underbrace{\frac{\mathrm{d}}{\mathrm{d}s}\bm{r}(s)}_{\mathclap{\text{Tangentenvektor an} \tilde{\mathcal{C}}}}.
\end{align*}
%
Das wiederum bedeutet, dass $\nabla\phi$ orthogonal zum Tangentenvektor an $\tilde{\mathcal{C}}$ ist, woraus folgt, dass $\frac{\nabla\phi}{|\nabla\phi|}$ die Flächennormale zur Fläche $\phi(\bm{r})= \mathrm{const}$ im Punkt $\bm{r}$ ist. Mit anderen Worten heißt das: Der Gradient steht stets senkrecht auf der Niveaufläche. Die geometrische Bedeutung des Gradienten liefert Abbildung~\ref{fig:2013-04-26.36}:

\begin{figure}[htpb]
	\centering
	\begin{pspicture}(-0.9,-2.7)(9,2.5)
		\pstThreeDPut(0,1,0){
			\pstThreeDEllipse[beginAngle=-20,endAngle=70](0,0,0)(0,3,0)(0,0,4)
			\pstThreeDEllipse[beginAngle=-20,endAngle=70](1,0,0)(0,3,0)(0,0,4)
			\pstThreeDLine(0,3 -15 cos mul,4 -15 sin mul)(1,3 -15 cos mul,4 -15 sin mul)
			\pstThreeDLine(0,3 64 cos mul,4 64 sin mul)(1,3 64 cos mul,4 64 sin mul)
			\pstThreeDNode(0.5,3 25 cos mul,4 25 sin mul){A1}
			\psdot*(A1)
		}
		\pstThreeDPut(0,5,1.5){
			\pstThreeDEllipse[beginAngle=-20,endAngle=70](0,0,0)(0,3,0)(0,0,4)
			\pstThreeDEllipse[beginAngle=-20,endAngle=70](1,0,0)(0,3,0)(0,0,4)
			\pstThreeDLine(0,3 -15 cos mul,4 -15 sin mul)(1,3 -15 cos mul,4 -15 sin mul)
			\pstThreeDLine(0,3 64 cos mul,4 64 sin mul)(1,3 64 cos mul,4 64 sin mul)
			\pstThreeDNode(0.5,3 25 cos mul,4 25 sin mul){A2}
			\psdot*(A2)
		}
		\pstThreeDPut(0,9,3.5){
			\pstThreeDEllipse[beginAngle=-20,endAngle=70](0,0,0)(0,3,0)(0,0,4)
			\pstThreeDEllipse[beginAngle=-20,endAngle=70](1,0,0)(0,3,0)(0,0,4)
			\pstThreeDLine(0,3 -15 cos mul,4 -15 sin mul)(1,3 -15 cos mul,4 -15 sin mul)
			\pstThreeDLine(0,3 64 cos mul,4 64 sin mul)(1,3 64 cos mul,4 64 sin mul)
			\pstThreeDNode(0.5,3 25 cos mul,4 25 sin mul){A3}
			\psdot*(A3)
		}
		\pstThreeDNode(0,0,1){A0}
		\pstThreeDNode(0,0,-2){O}
		\pscurve[linecolor=MidnightBlue](A0)(A1)(A2)(A3)
		\pcline[linecolor=DarkOrange3,arrows=->](O)(A0)\naput{\color{DarkOrange3} $\bm{r}(\bm{s})$}
		\pcline[linecolor=DarkOrange3,arrows=->](O)(A1)\naput{\color{DarkOrange3} $\bm{r}_1(\bm{s})$}
		\pcline[linecolor=DarkOrange3,arrows=->](O)(A2)\naput{\color{DarkOrange3} $\bm{r}_2(\bm{s})$}
		\pcline[linecolor=DarkOrange3,arrows=->](O)(A3)\nbput{\color{DarkOrange3} $\bm{r}_3(\bm{s})$}
		\uput[90](A1){\color{DimGray} $\phi_1(s_1)$}
		\uput[90](A2){\color{DimGray} $\phi_2(s_2)$}
		\uput[90](A3){\color{DimGray} $\phi_3(s_3)$}
		\uput[45](A0){\color{MidnightBlue} $\mathcal{C}$}
	\end{pspicture}
	\caption{Illustration des Gradienten. Der Gradient in blau steht stets senkrecht auf der grauen Niveaufläche.}
	\label{fig:2013-04-26.36}
\end{figure}

Der Gradient gibt nämlich durch seine Richtung die Richtung des steilsten Anstiegs der Funktion $\phi(\bm{r})$ und durch seinen Betrag die Stärke dieses Anstieges an.

Eingedenk der koordinatenunabhängigen Definition des Gradienten lässt sich ein \acct*{wichtiger Satz} folgern:
%
\begin{align*}
	\int_{\mathcal{C}: s_0}^{s_1} \nabla\phi\cdot\mathrm{d}\bm{r} &= \int_{\phi(s_0)}^{\phi(s_1)} \mathrm{d}\phi = \phi(s_1) - \phi(s_0).
\end{align*}
%
\begin{figure}[htpb]
	\centering
	\begin{pspicture}(-0.6,-0.2)(3.5,2.2)
		\psbcurve(0,0)(1.5,1)L(2,0)(3,2)
		\psdots*[linecolor=MidnightBlue](0,0)(3,2)
		\pcline[linecolor=DarkOrange3,arrows=->](1,0)(1.5,1)
		\nbput{\color{DarkOrange3} $\bm{r}(\bm{s})$}
		\uput[45](1.5,1){\color{DimGray} $\mathcal{C}$}
		\uput[180](0,0){\color{MidnightBlue} $s_0$}
		\uput[0](3,2){\color{MidnightBlue} $s_1$}
	\end{pspicture}
	\caption{Das Kurvenintegral hängt lediglich von Anfangs- und Endpunkt ab.}
\end{figure}
%
Das bedeutet, dass das Kurvenintegral lediglich vom Anfangs- und Endpunkt abhängt, nicht jedoch vom Weg der dazwischen liegt. In Zylinderkoordinaten lautet der Gradient wie folgt:
%
\begin{align*}
	\nabla\phi &= \left(\bm{e}_{\varrho}\frac{\partial}{\partial\varrho} + \bm{e}_{\varphi}\frac{1}{\varrho}\frac{\partial}{\partial\varphi} + \bm{e}_z \frac{\partial}{\partial z}\right)
\end{align*}
%
\subsection{Quellstärke (Divergenz), Gaußscher Satz}
%
Die \acct*{Divergenz}\index{Differentialoperator!Divergenz} ist eine Abbildung, die einem Vektorfeld ein skalares Feld zuordnet, das heißt:
%
\begin{align*}
	\div : \bm{A}(\bm{r}) \to \div\bm{A}(\bm{r}).
\end{align*}
%
Dieser Sachverhalt schreibt sich in kartesischen Koordinaten
%
\begin{align*}
	\div\bm{A}(\bm{r}) &= \frac{\partial}{\partial x} A_x + \frac{\partial}{\partial y} A_y + \frac{\partial}{\partial z} A_z 
\intertext{und in Zylinderkoordinaten}
	\div\bm{A}(\bm{r}) &= \frac{1}{\varrho}\frac{\partial}{\partial\varrho}\left(\varrho A_{\varrho}\right) + \frac{1}{\varrho}\frac{\partial}{\partial\varphi}A_{\varphi} + \frac{\partial}{\partial z} A_z.
\end{align*}
%
Darüber hinaus gibt es, wie auch beim Gradient, eine koordinatenfreie Definition von $\div\bm{A}$:
%
\begin{align*}
	\div\bm{A} &\coloneq \lim\limits_{\Delta V\to 0}\left[\frac{1}{\Delta V}\int_{\partial\Delta V} \bm{A}\cdot\mathrm{d}\bm{f}\right].
\end{align*}

\begin{figure}[htpb]
	\centering
	\begin{pspicture}(0,0)(5,3)
		\pstThreeDBox[linecolor=MidnightBlue](-3,1,1)(1,0,0)(0,1,0)(0,0,0.5)
		\pstThreeDLine[linecolor=DarkOrange3,arrows=->](0,0,0)(-2,1.5,1.25)
		\pstThreeDDot(0,0,0)
		\pstThreeDLine[linecolor=Purple,arrows=->](-2.5,1.5,1.5)(-2.5,1.5,2.5)
		\pstThreeDNode(-2.5,1.5,2.5){n}
		\pstThreeDNode(-1,0.75,0.625){r}
		\pstThreeDNode(-3,2,1.5){V}
		\psline[arrows=->](1,3)(1.5,2.5)
		\psline[arrows=->](1,0)(1.5,0.5)
		\psline[arrows=->](4.5,0.5)(5,0)
		\psline[arrows=->](4.5,2.5)(5,3)
		\uput[130](r){\color{DarkOrange3} $\bm{r}$}
		\uput[-30](V){\color{MidnightBlue} $\Delta V$}
		\uput[0](n){\color{Purple} $\bm{n}$}
	\end{pspicture}
	\caption{Illustration zur Divergenz.}
\end{figure}

Mit dieser Definition wird der \acct*{Gaußsche Satz}\index{Gaußscher Satz} eingängiger. Dieser lautet:
%
\begin{theorem}[Gaußscher Satz]
	\begin{align*}
		\int_{\partial V} \bm{A}\cdot\mathrm{d}\bm{f} &= \int_{V} \div\bm{A}\,\mathrm{d}V.
	\end{align*}

	\begin{proof}[Beweisidee]
		\begin{figure}[htpb]
			\centering
			\def\x{0.3}
			\begin{pspicture}[coorType=2](0,-0.5)(3,1.5)
				\psccurve[linecolor=MidnightBlue](0,0)(1,0)(2,-0.5)(3,1)(2,1)(1,1.5)
				\psellipse[linecolor=DarkOrange3,fillstyle=hlines,hatchcolor=DarkOrange3,hatchsep=2pt](2,0.5)(0.25,0.125)
				\psline[linecolor=DarkOrange3,arrows=->](2,0.5)(2,1.5)
				\uput[0](2,1.5){\color{DarkOrange3} $\bm{n}$}
				\rput(1,1.2){\color{MidnightBlue} $V$}
				\pcarc(1,0)(0.5,-0.3)
				\uput{0.1}[180](0.5,-0.3){\color{MidnightBlue} $\partial V$}
				\rput(0.5,0.3){
					\pstThreeDBox(0,0,0)(0,0,\x)(\x,0,0)(0,\x,0)
					\pstThreeDBox(0,\x,0)(0,0,\x)(\x,0,0)(0,\x,0)
					\pstThreeDBox(0,\x,\x)(0,0,\x)(\x,0,0)(0,\x,0)
					\pstThreeDBox(0,2 \x\space mul,\x)(0,0,\x)(\x,0,0)(0,\x,0)
				}
			\end{pspicture}
			\caption{Zum Beweis des Gaußschen Satzes.}
		\end{figure}

		Da die Grundflächen der Kuben sich innerhalb des Volumens $V$ im Oberflächenintegral gegenseitig aufheben bleiben nur noch die Flächen außen am Rand $\partial V$ übrig.
	\end{proof}
\end{theorem}

\paragraph{Anwendungen des Gaußschen Satzes}
%
Zum Einen lassen sich das Gaußsche- und Gil\-bert\-sche- Gesetz, also die ersten beiden Feldgleichungen in ihrer integralen Form, in ihre differentielle Form überführen, denn:
%
\begin{align*}
	4\pi\int_V \varrho\,\mathrm{d}V &= \int_{\partial V} \bm{E}\cdot\mathrm{d}\bm{f} = \int_V \div\bm{E}\,\mathrm{d}V\\
	\implies \div\bm{E} &= 4\pi\varrho\\
	\int_{\partial V} \bm{B}\cdot\mathrm{d}\bm{f} &= 0 = \int_V \div\bm{B}\,\mathrm{d}V\\
	\implies \div\bm{B} &= 0.
\end{align*} 
%
Zum Anderen lässt sich die Kontinuitätsgleichung von ihrer integralen Form auf ihre differentielle Form\index{Kontinuitätsgleichung!differentielle Form} überführen:
%
\begin{align*}
	\int_V \partial_t\varrho\,\mathrm{d}V &= -\int_{\partial V} \bm{j}\cdot\mathrm{d}\bm{f}\\
	\implies \int_V \partial_t\varrho\,\mathrm{d}V &= -\int_V \div\bm{j}\,\mathrm{d}V\\
	\implies \partial_t\varrho + \div\bm{j} &= 0
\end{align*}
%
Damit haben wir eine interessante Aussage gewonnen. Denn die Kontinuitätsgleichung in ihrer integralen Form ist global gültig. Durch Anwenden des Gaußschen Satzes und Umwandeln in ihre differentielle Form zeigt sich auch ihre punktweise Gültigkeit. Diese Argumentation lässt sich jedoch auch umdrehen. Durch Integrieren der erhaltenen Differentialgleichung kommen wir wieder auf die integrale Form und erhalten damit aus der punktweisen Gültigkeit die globale Gültigkeit der Gleichung.
%
\subsection{Wirbelstärke (Rotation), Stokesscher Satz}
%
Die \acct*{Rotation}\index{Differentialoperator!Rotation} ist eine Abbildung von einem Vektorfeld auf ein Vektorfeld, d.h.
%
\begin{align*}
	\rot : \bm{A}(\bm{r}) \to \rot\bm{A}(\bm{r}).
\end{align*}
%
Die Rotation ist in kartesischen Koordinaten über
%
\begin{align*}
	\rot\bm{A}(\bm{r}) &= \nabla\times\bm{A}(\bm{r}) = \left(\partial_y A_z - \partial_z A_y\right)\bm{e}_x + \left(\partial_z A_x - \partial_x A_z\right)\bm{e}_y + \left(\partial_x A_y - \partial_y A_x\right)\bm{e}_z\\
	\intertext{bzw.\ mit dem Levi-Civita-Symbol:}
	(\rot\bm{A})_{\alpha} &= \varepsilon_{\alpha\beta\gamma}\partial_{\beta}A_{\gamma}\\
	\intertext{und in Zylinderkoordinaten über}
	\rot\bm{A} &= \left(\frac{1}{\varrho}\partial_{\varphi}A_z - \partial_z A_{\varphi}\right)\bm{e}_{\varrho} + \left(\partial_z A_{\varrho} - \partial_{\varrho}A_z\right)\bm{e}_{\varphi} + \left(\frac{1}{\varrho}\partial_{\varrho}(\varrho A_{\varphi}) - \frac{1}{\varrho}\partial_{\varphi}A_{\varrho}\right)\bm{e}_z
\end{align*}
%
definiert. Auch hier gibt es wieder eine koordinatenfreie Definition:
%
\begin{align*}
	\bm{n}\cdot\rot\bm{A}(\bm{r}) & \coloneq \lim \frac{1}{\Delta F}\underbrace{\int_{\partial\Delta F} \bm{A}(\bm{r})\cdot\mathrm{d}\bm{r}}_{\Gamma_{\mathcal{C}}(\bm{A})}.
\end{align*}

\begin{figure}[htpb]
	\centering
	\def\x{1}
	\begin{pspicture}(-1.6,-1.7)(2.6,1.5)
		\pstThreeDLine[linecolor=DarkOrange3,arrows=->](2,0,-1)(0,0,0)
		\pstThreeDDot(2,0,-1)
		\pscustom[fillstyle=hlines]{
			\pstThreeDLine[linestyle=none](-\x,\x,0)(-\x,-\x,0)
			\pstThreeDLine[linestyle=none](-\x,-\x,0)(\x,-\x,0)
			\pstThreeDLine[linestyle=none](\x,-\x,0)(\x,\x,0)
			\pstThreeDLine[linestyle=none](\x,\x,0)(-\x,\x,0)
		}
		\pstThreeDLine[linecolor=MidnightBlue,ArrowInside=->](-\x,\x,0)(-\x,-\x,0)
		\pstThreeDLine[linecolor=MidnightBlue,ArrowInside=->](-\x,-\x,0)(\x,-\x,0)
		\pstThreeDLine[linecolor=MidnightBlue,ArrowInside=->](\x,-\x,0)(\x,\x,0)
		\pstThreeDLine[linecolor=MidnightBlue,ArrowInside=->](\x,\x,0)(-\x,\x,0)
		\pstThreeDLine[linecolor=Purple,arrows=->](0,0,0)(0,0,1.5)
		\pstThreeDNode(0,0,1.5){n}
		\pstThreeDNode(1,0,-0.5){r}
		\pstThreeDNode(-\x\space 2 div,\x\space 2 div,0){F}
		\pstThreeDNode(0,\x,0){dF}
		\uput[-45](r){\color{DarkOrange3} $\bm{r}$}
		\uput[0](n){\color{Purple} $\bm{n}$}
		\pcarc(F)([nodesep=1]F)\uput[0]([nodesep=1]F){\color{DimGray} $F$}
		\pcarc(dF)([nodesep=0.5,offset=-0.5]dF)\uput[0]([nodesep=0.5,offset=-0.5]dF){\color{MidnightBlue} $\partial F$}
	\end{pspicture}
	\caption{Illustration zur Rotation.}
\end{figure}

Damit wird der \acct*{Stokessche Satz}\index{Stokesscher Satz} griffiger.

\begin{theorem}[Stokesscher Satz]
%
\begin{align*}
	\int_F \rot\bm{A}\cdot\mathrm{d}\bm{f} &= \int_{\partial F} \bm{A}\cdot\mathrm{d}\bm{r}
\end{align*}
%
\begin{proof}[Beweisidee]

\begin{figure}[htpb]
	\centering
	\begin{pspicture}(-0.5,-1.5)(4.5,2)
		\pscurve[linecolor=DarkOrange3](0.5,0.5)(0,0.25)(0.5,0)(0.5,-0.25)(1,-0.5)
		\psecurve[linecolor=DarkOrange3,linestyle=dotted,dotsep=1pt](0,0.25)(0.5,0.5)(2,0)(1,-0.5)(0.5,-0.25)
		\psecurve(0,0.25)(0.5,0.5)
		(2,1.5)(2.5,1.4)(3,1.5)(4,1)(3.8,0)(4,-1)(2.5,-1.3)(2,-1.2)(1.5,-1.3)
		(1,-0.5)(0.5,-0.25)
		\psellipse[linecolor=MidnightBlue,fillstyle=hlines,hatchcolor=MidnightBlue](3,1)(0.5,0.25)
		\psline[linecolor=MidnightBlue,arrows=->](3,1)(3.3,2)
		\uput[180](3.3,2){\color{MidnightBlue} $\bm{n}$}
		\rput(1.5,-1){\color{DimGray} $F$}
		\uput{0.5}[20](0.5,0.5){\color{DarkOrange3} $\partial F = \mathcal{C}$}
		\rput(2.5,0.5){
			\pscurve(0,0)(0.5,-0.2)(1,0)
			\pscurve(0,-0.5)(0.5,-0.7)(1,-0.5)
			\pscurve(0,-1)(0.5,-1.2)(1,-1)
			\pscurve(0.2,0.2)(0.1,-0.5)(0.2,-1.4)
			\pscurve(0.5,0.2)(0.4,-0.5)(0.5,-1.4)
			\pscurve(0.8,0.2)(0.7,-0.5)(0.8,-1.4)
			\psline[linecolor=DarkGreen,linestyle=none,arrows=->](0,-0.5)(0.3,-0.65)
			\psline[linecolor=DarkGreen,linestyle=none,arrows=->](0.2,0.2)(0.1,-0.3)
			\psline[linecolor=DarkGreen,linestyle=none,arrows=>-](0.4,-0.4)(0.5,0.2)
			\psline[linecolor=DarkGreen,linestyle=none,arrows=>-](0.3,-0.15)(0,0)
			\psline[linecolor=Purple,linestyle=none,arrows=>-](0.4,-0.7)(0,-0.5)
			\psline[linecolor=Purple,linestyle=none,arrows=<-](0.3,-1.15)(0,-1)
			\psline[linecolor=Purple,linestyle=none,arrows=-<](0.2,-1.4)(0.1,-0.8)
			\psline[linecolor=Purple,linestyle=none,arrows=->](0.5,-1.4)(0.4,-0.8)
		}
	\end{pspicture}
	\caption{Zum Beweis des Stokesschen Satzes.}
\end{figure}

Im Inneren der Fläche $F$ heben sich paarweise immer die nebeneinander liegenden Linien des >>Netzes<< auf (bzw.\ die Linienintegrale heben sich auf). Betrachten wir dies kontinuierlich bis zum Rand $\partial F$ so bleiben zu Letzt nur noch die Linienintegrale am Rand erhalten.
\end{proof}
\end{theorem}

Sei $F= \partial V$, das heißt $\partial F = \partial\partial V = 0$, also $F$ ist geschlossen; dann gilt:
%
\begin{align*}
	\int_{F= \partial V} \left(\nabla\times\bm{A}\right)\cdot\mathrm{d}\bm{f} &= 0.
\end{align*}
%
Die Fläche $F$ darf bei fester Raumkurve $\partial F$ im Definitionsbereich von $\rot\bm{A}$ beliebig deformiert werden. Wenn wir darauf den Gaußschen Satz anwenden, dann folgt:
%
\begin{align*}
	0 = \int_{F= \partial V} \left(\rot\bm{A}\right)\cdot\mathrm{d}\bm{f} &= \int_{V} \div\left(\rot\bm{A}\right)\,\mathrm{d}V\\
	\implies \div\rot &\equiv 0
\end{align*}
%
Das heißt, das Wirbel quellenfrei sind.

Sei weiterhin $\bm{A}= \nabla\phi$, dann folgt mit dem Stokesschen Satz:
%
\begin{align*}
	\int_{F} \rot\left(\grad\phi\right)\cdot\mathrm{d}\bm{f} &= \int_{\partial F} \left(\grad\phi\right)\cdot\mathrm{d}\bm{r} = 0\\
	\intertext{da $\partial F$ eine geschlossene Kurve ist, d.h. $\partial\partial F = 0$}
	\implies \rot\grad = 0,
\end{align*}
%
was wiederum bedeutet, dass Gradientenfelder wirbelfrei sind.

\begin{notice}
	\begin{figure}[htpb]
		\centering
		\begin{pspicture}(-0.5,-0.6)(4.5,1.5)
			\pscurve(1,1)(0.8,0.5)(0.5,0)(2,-0.6)(3,-0.5)
			\pscurve(3,0.5)(2.2,0.5)(2,1)
			\psellipse[linestyle=none,fillstyle=hlines](1.5,1)(0.5,0.25)
			\psellipse[linestyle=none,fillstyle=hlines](3,0)(0.25,0.5)
			\psellipticarcn[linecolor=Purple,arrows=->](1.5,1)(0.5,0.25){90}{-90}
			\psellipticarcn[linecolor=Purple,arrows=->](1.5,1)(0.5,0.25){-90}{90}
			\psellipticarcn[linecolor=Purple,arrows=->](3,0)(0.25,0.5){0}{-180}
			\psellipticarcn[linecolor=Purple,arrows=->](3,0)(0.25,0.5){180}{0}
			\rput(2,0){\color{DimGray} $F$}
			\psline[linecolor=MidnightBlue,arrows=->](1,0)(0,1)
			\uput[180](0,1){\color{MidnightBlue} $\bm{n}$}
			\pcarc(1.5,1)(4,1.3)
			\pcarc(3,0)(4,1.3)
			\uput[0](4,1.3){\color{DimGray} $\partial F$}
		\end{pspicture}
		\hspace{0.2\textwidth}
		\begin{pspicture}(-1,-0.6)(1.5,1.5)
			\psellipse[linestyle=none,fillstyle=hlines](0,0)(1,0.5)
			\psellipse[linestyle=none,fillstyle=solid,fillcolor=white](0,0)(0.5,0.25)
			\psellipticarcn[linecolor=Purple,arrows=->](0,0)(0.5,0.25){90}{-90}
			\psellipticarcn[linecolor=Purple,arrows=->](0,0)(0.5,0.25){-90}{90}
			\psellipticarc[linecolor=Purple,arrows=->](0,0)(1,0.5){90}{-90}
			\psellipticarc[linecolor=Purple,arrows=->](0,0)(1,0.5){-90}{90}
			\pcarc(-0.75,0)(-0.5,1)
			\uput[60](-0.5,1){\color{DimGray} $F$}
			\psline[linecolor=MidnightBlue,arrows=->](0.75,0)(0.8,1)
			\uput[0](0.8,1){\color{MidnightBlue} $\bm{n}$}
		\end{pspicture}
		\caption{Die Ränder dieser Gebiete sind nicht einfach zusammenhängend.}
	\end{figure}
	%
	Es gilt stets zu beachten, dass sich Ränder von Flächen stets aus mehreren Stücken zusammensetzen können. Man spricht in solchen Fällen von \acct*{nicht einfach zusammenhängenden} Mannigfaltigkeiten.
\end{notice}

\begin{theorem}[Wichtige Lemmata]
	%
	\begin{description}
		\item[(L1)] In einem \acct*{einfach zusammenhängenden Gebiet} gilt:
			\begin{align*}
				\rot\bm{A} = 0 \iff \exists\, \phi(\bm{r}) : \bm{A} = \grad\phi
			\end{align*}
			Dabei ist das skalare Feld $\phi(\bm{r})$ eindeutig bis auf eine Konstante. Das Vektorpotential $\bm{A}$ besitzt dann eine sogenannte \acct{Eichfreiheit}.

			\begin{figure}[htpb]
				\centering
				\begin{pspicture}(-0.6,-0.2)(3.5,2.2)
					\psccurve(0,1.5)(1,1)(3,2)(2,0)
					\uput[90](1,1){\color{DimGray} $g$}
				\end{pspicture}
				\hspace{0.1\textwidth}
				\begin{pspicture}(-0.6,-0.2)(3.5,2.2)
					\psccurve(0,1.5)(1,1.3)(3,2)(2,0)
					\psellipse[fillstyle=solid,fillcolor=white](2.3,1)(0.5,0.25)
					\psellipticarc[linecolor=Purple,arrows=->](2.3,1)(0.6,0.3){-90}{90}
					\psellipticarc[linecolor=Purple,arrows=->](2.3,1)(0.6,0.3){90}{-90}
					\psarc[linecolor=Purple,arrows=->](1,0.8){0.3}{-90}{90}
					\psarc[linecolor=Purple,arrows=->](1,0.8){0.3}{90}{-90}
					\uput[90](1,1.3){\color{DimGray} $g$}
				\end{pspicture}
				\caption{\textbf{Links:} Einfach zusammenhängendes Gebiet. \textbf{Rechts:} Zweifach zusammenhängendes Gebiet.}
			\end{figure}

		\item[(L2)]
			\begin{itemalign}
				\div\bm{V}= 0 \iff \exists\,\bm{A}:\bm{V}= \rot\bm{A}
			\end{itemalign}
		Das Vektorfeld $\bm{V}$ ist dabei >>eindeutig<< bis auf $\nabla\phi$.
	\end{description}
	%
\end{theorem}

Als \acct*{einfach zusammenhängendes Gebiet} wird jede geschlossene Kurve im Gebiet $g$ bezeichnet, die folgende Eigenschaft hat:
%
\begin{align*}
	g: \mathcal{C} \to \text{Punkt} \in g.
\end{align*}
%
Das heißt die Kurve $\mathcal{C}$ kann stetig auf einen einzigen Punkt, der wiederum im Gebiet $g$ liegt zusammengezogen werden. Als \acct*{zweifach zusammenhängend}, bezeichnet man ein Loch (oder auch einen Punkt), wo $\bm{A}$ nicht definiert ist. Dort kann $\rot\bm{A}= 0$ in $g$ sein, zugleich aber
%
\begin{align*}
	\int_{\mathcal{C}} \bm{A}\cdot\mathrm{d}\bm{r}\neq 0
\end{align*}
%
sein.
%
\paragraph{Anwendungen des Stokesschen Satzes}
%
Der Stokessche Satz kann, wie auch schon der Gaußsche Satz, dazu verwendet werden die Maxwellgleichungen von ihrer integralen Form auf ihre differentielle Form zu bringen.

Betrachten wir zunächst das Oerstedtsche., bzw. Ampèrsche- Gesetz mit der zugehörigen Maxwellschen Ergänzung:
%
\begin{align*}
	\int_{F}\left(\frac{4\pi}{c}\bm{j} + \frac{1}{c}\partial_t\bm{E}\right)\cdot\mathrm{d}\bm{f} &= \int_{\partial F} \bm{B}\cdot\mathrm{d}\bm{r}= \int_{F} \rot\bm{B}\cdot\mathrm{d}\bm{f}\\
	\implies \rot\bm{B} &= \frac{4\pi}{c}\bm{j} + \frac{1}{c}\partial_t\bm{E}
\end{align*}
%
Darüber hinaus können wir das Induktionsgesetz umformen. Dazu betrachten wir zunächst eine ruhende, feste Schleife $\mathcal{C}= \partial F$, das heißt $\bm{v}= 0$ im Induktionsgesetz.
%
\begin{align*}
	\implies -\frac{1}{c}\frac{\mathrm{d}}{\mathrm{d}t}\int_{F} \bm{B}\cdot\mathrm{d}\bm{f} &= -\frac{1}{c}\int_{F} \partial_t\bm{B}\cdot\mathrm{d}\bm{f} = \int_{\partial F} \bm{E}\cdot\mathrm{d}\bm{r} = \int_{F} \rot\bm{E}\cdot\mathrm{d}\bm{f}\\
	\implies \rot\bm{E} &= -\frac{1}{c}\partial_t\bm{B}
\end{align*}
%
\begin{notice}[Nebenrechnung:]
	\begin{align*}
		I_t &\coloneq \frac{\mathrm{d}}{\mathrm{d}t}\int_{F_t} \bm{A}(\bm{r},t)\cdot\mathrm{d}\bm{f}\\
		I_t &= \lim\limits_{\tau\to 0}\frac{1}{\tau}\Biggl[\int_{F_{t+\tau}} \bm{A}(t + \tau)\cdot\mathrm{d}\bm{f} - \int_{F_t}\bm{A}(t)\cdot\mathrm{d}\bm{f}\Biggr]\\
		&= \lim\limits_{\tau\to 0}\frac{1}{\tau}\Biggl[\int_{F_{t+\tau}} \bm{A}(t + \tau)\cdot\mathrm{d}\bm{f} - \int_{F_t}\bm{A}(t + \tau)\cdot\mathrm{d}\bm{f} + \int_{F_t}\underbrace{\left(\bm{A}(t + \tau) - \bm{A}(t)\right)}_{\tau\partial_t\bm{A}(t) + o(\tau^2)}\cdot\mathrm{d}\bm{f}\Biggr]\\
		&=\lim\limits_{\tau\to 0}\frac{1}{\tau}\Biggl[\int_{F_{t + \tau}}\left[\bm{A}(t) + \tau\partial_t\bm{A}(t)\right]\cdot\mathrm{d}\bm{f} - \int_{F_t}\left[\bm{A}(t) + \tau\partial_t\bm{A}(t)\right]\cdot\mathrm{d}\bm{f} + o(\tau^2)\Biggr]\\
		&= \lim\limits_{\tau\to 0}\frac{1}{\tau}\Biggl[\int_{F_{t + \tau}} \bm{A}(t)\cdot\mathrm{d}\bm{f} - \int_{F_t} \bm{A}(t)\,\mathrm{d}\bm{f} + \underbrace{\tau\underbrace{\left(\int_{F_{t + \tau}} \partial_t\bm{A}\cdot\mathrm{d}\bm{f} - \int_{F_t} \partial_t\bm{A}\cdot\mathrm{d}\bm{f}\right)}_{o(\tau)}}_{o(\tau^2)}\Biggr]\\
		I_t &= \lim\limits_{\tau\to 0}\frac{1}{\tau}\Biggl[\int_{F_{t + \tau}} \bm{A}(t)\cdot\mathrm{d}\bm{f} - \int_{F_t} \bm{A}(t)\cdot\mathrm{d}\bm{f}\Biggr] + \int_{F_t} \partial_t\bm{A}(t)\cdot\mathrm{d}\bm{f}
	\end{align*}
\end{notice}
