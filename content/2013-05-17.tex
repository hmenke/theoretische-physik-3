% Henri Menke, Michael Schmid, Marcel Klett, Jan Schnabel 2013 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

% Vorlesung am 10.04.2013
% TeX: Jan

\renewcommand{\printfile}{2013-05-17}

\paragraph{Abhängigkeit vom Bezugssystem:}
%
Wir betrachten folgende Situation:
%
\begin{align*}
	\bm{r}' &= \bm{r}'' + \bm{a},\qquad \varrho(\bm{r}') = \varrho(\bm{r}'' + \bm{a}) = \varrho_a(\bm{r}'')\\
	Q_{\alpha\beta} &= \int\varrho_a(\bm{r}'')\left[3(x''_{\alpha} + a_{\alpha})(x''_{\beta} + a_{\beta}) - (\bm{r}'' + \bm{a})^2\delta_{\alpha\beta}\right]\,\mathrm{d}V''\\
	&= \!
	\begin{multlined}[t]
		\int\varrho_a(\bm{r}'')\Bigl[ \underline{3x''_{\alpha}x''_{\beta}} + 3a_{\alpha}x''_{\beta} + 3a_{\beta}x''_{\alpha} + 3a_{\alpha}a_{\beta} - \underline{r''^2\delta_{\alpha\beta}} \\
		- 2\bm{r}''\cdot\bm{a}\delta_{\alpha\beta} - a^2\delta_{\alpha\beta} \Bigr]\,\mathrm{d}V''
	\end{multlined}
\intertext{fassen wir dies nun zusammen und nutzen die Definition den elektrischen Dipolmoments aus, so erhalten wir:}
	&= Q^{(a)}_{\alpha\beta} + 3a_{\alpha}p^{(a)}_{\beta} + 3a_{\beta}p^{(a)}_{\alpha} + \left(3a_{\alpha}a_{\beta} - a^2\delta_{\alpha\beta}\right)q_{\text{tot}} - 2\delta_{\alpha\beta}\bm{a}\cdot\bm{p}^{(a)}
\end{align*}
%
Daraus folgt, dass wenn $q_{\text{tot}}= 0 = \bm{p}$ ist, dann ist $Q_{\alpha\beta}$ unabhängig vom Bezugssystem.

Wir wollen uns nun noch einige Eigenschaften des Quadrupoltensors $Q$ vergegenwärtigen:
%
\begin{align*}
	Q &=
	\begin{pmatrix}
		Q_{11} & Q_{12} & Q_{13}\\
		Q_{21} & Q_{22} & Q_{23}\\
		Q_{31} & Q_{32} & Q_{33}
	\end{pmatrix}
\end{align*}
%
\begin{itemize}
	\item Der Quadrupoltensor ist symmetrisch, d.h.\ es gilt $Q_{\alpha\beta} = Q_{\beta\alpha}$, damit haben wir $9 - 3 = 6$ unabhängige (reelle) Elemente
	\item Der Quadrupoltensor ist spurfrei, d.h.\ $Q_{\alpha\alpha} = \tr (Q) = 0$, damit haben wir nur noch $6 - 1 = 5$ unabhängige Elemente
	\item Der Quadrupoltensor $Q$ kann durch eine orthogonale Transformation des Koordinatensystems, was einer Drehung entspricht, auf >>Hauptachsen<< transformiert, d.h.\ diagonalisiert werden. Er nimmt dann folgende Form an:
	%
	\begin{align*}
		Q &\xrightarrow{\text{Drehung}}
		\begin{pmatrix}
			Q'_{11} & 0 & 0\\
			0 & Q'_{22} & 0\\
			0 & 0 & Q'_{33}
		\end{pmatrix}.
	\end{align*}
	%
	Dabei ist $Q'_{\alpha\alpha}= 0$, wegen der Invarianz der Spur bei orthogonalen Transformationen. Damit haben wir in Diagonalform lediglich zwei unabhängige Elemente. Im Allgemeinen aber legen zwei Matrixelemente die >>Stärke<< fest und drei Matrixelemente die >>Orientierung<<. 
\end{itemize}

\paragraph{Ersatzladungsverteilung}
%
Wir untersuchen auch hier, analog zum Monopol- und Dipolpotential die Ersatzladungsverteilung beim elektrischen Quadrupolpotential. Hierzu betrachten wir die zugehörige Poissongleichung.
%
\begin{align*}
	\nabla^2\phi_3 &= -4\pi\varrho_3 = \frac{1}{6}Q_{\alpha\beta}\partial_{\alpha}\partial_{\beta}\nabla^2\frac{1}{r} = -4\pi\frac{1}{6}Q_{\alpha\beta}\partial_{\alpha}\partial_{\beta}\delta(\bm{r})\\
	\implies \varrho_3(\bm{r}) &= \frac{1}{6}Q_{\alpha\beta}\partial_{\alpha}\partial_{\beta}\delta(\bm{r})
\end{align*}

\begin{notice}
	Im Gegensatz zu $\varrho_1$ und $\varrho_2$ sind für $\varrho_3$ die Ersatzladungen und dessen Lagen durch $Q_{\alpha\beta}$ nicht mehr \acct*{eindeutig} bestimmt.

	Im Allgemeinen benötigt man mindestens vier Ladungen $q_i$, um einen Quadrupol zu erzeugen, denn:

	Zur Vermeidung eines Mono- oder eines Dipolfeldes muss gelten:
	%
	\begin{align*}
		\sum\limits_{i}q_i &= 0,\;\text{und}\;\sum\limits_{i}q_i\bm{r}_i = 0.
	\end{align*}
	%
	Wir haben also vier Gleichungen, zu deren Erfüllung man vier $q_i$ benötigt.
\end{notice}

\subsubsection{Beispiele}

\begin{figure}[htpb]
	\centering
	\def\i{1.5}
	\begin{pspicture}(-2,-2)(2,2)
		\psaxes[labels=none,ticks=none,arrows=->](0,0)(-2,-2)(2,2)[{\color{DimGray} $x$}, -90][{\color{DimGray} $y$}, 0]
		\psdots*[linecolor=DarkOrange3](\i,0)(-\i,0)
		\psdots*[linecolor=MidnightBlue](0,\i)(0,-\i)
		\uput[90](\i,0){\color{DarkOrange3} $q$}
		\uput[90](-\i,0){\color{DarkOrange3} $q$}
		\uput[0](0,\i){\color{MidnightBlue} $-q$}
		\uput[0](0,-\i){\color{MidnightBlue} $-q$}
		\psbrace[
			fillcolor=DimGray,
			braceWidth=0.5pt,
			braceWidthInner=4pt,
			braceWidthOuter=2pt,
			ref=lC,
			rot=90,
			nodesepB=5pt
		](0,0)(\i,0){\color{DimGray} \clap{$a$}}
		\psbrace[
			fillcolor=DimGray,
			braceWidth=0.5pt,
			braceWidthInner=4pt,
			braceWidthOuter=2pt,
			ref=lC,
			rot=90,
			nodesepB=5pt
		](-\i,0)(0,0){\color{DimGray} \clap{$a$}}
		\psbrace[
			fillcolor=DimGray,
			braceWidth=0.5pt,
			braceWidthInner=4pt,
			braceWidthOuter=2pt,
			ref=lC,
			rot=0,
			nodesepA=5pt
		](0,0)(0,\i){\color{DimGray} \clap{$b$}}
		\psbrace[
			fillcolor=DimGray,
			braceWidth=0.5pt,
			braceWidthInner=4pt,
			braceWidthOuter=2pt,
			ref=lC,
			rot=0,
			nodesepA=5pt
		](0,-\i)(0,0){\color{DimGray} \clap{$b$}}
	\end{pspicture}
	\hspace{0.2\textwidth}
	\begin{pspicture}(-0.7,-2)(1,2)
		\psaxes[xAxis=false,labels=none,ticks=none,arrows=->](0,0)(-0.7,-2)(1,2)[,0][{\color{DimGray} $y$}, 0]
		\psdots*[linecolor=DarkOrange3](0,\i)(0,-\i)
		\psdot*[dotscale=1.5,linecolor=MidnightBlue](0,0)
		\uput[0](0,0){\color{MidnightBlue} $-2q$}
		\uput[0](0,\i){\color{DarkOrange3} $q$}
		\uput[0](0,-\i){\color{DarkOrange3} $q$}
		\psbrace[
			fillcolor=DimGray,
			braceWidth=0.5pt,
			braceWidthInner=4pt,
			braceWidthOuter=2pt,
			ref=lC,
			rot=180,
			nodesepA=-5pt
		](0,\i)(0,0){\color{DimGray} \clap{$b$}}
		\psbrace[
			fillcolor=DimGray,
			braceWidth=0.5pt,
			braceWidthInner=4pt,
			braceWidthOuter=2pt,
			ref=lC,
			rot=180,
			nodesepA=-5pt
		](0,0)(0,-\i){\color{DimGray} \clap{$b$}}
	\end{pspicture}
	\caption{\textbf{Links:} Eine mögliche Anordnung eines Quadrupols. \textbf{Rechts:} Getreckter Quadrupol.}
\end{figure}

\begin{example}
	Allgemein: Im Fall einer \emph{rotationsinvarianten} Ladungsverteilung kann der Quadrupoltensor $Q$ stets geschrieben werden als:
	%
	\begin{align*}
		Q &=
		\begin{pmatrix}
			-Q & 0 & 0\\
			0 & -Q & 0\\
			0 & 0 & 2Q
		\end{pmatrix}
		\intertext{dabei heißt}
		\frac{1}{2}Q_{33} &= -Q
	\end{align*}
	%
	Quadrupolmoment. Rotationsinvariant heißt dabei, dass $Q_{xx} = Q_{yy}$, womit $Q_{zz}$ wegen $\tr (Q) = 0$ festliegt. Weiterhin gilt:
	%
	\begin{align*}
		Q_{zz} &= \int\varrho(\bm{r}')\left[3z'^2 - r'^2\right]\,\mathrm{d}V'\underset{\varrho = \text{const} > 0}{{}={}} - \varrho\int_{V}\left[x^2 + y^2 - 2z^2\right]\,\mathrm{d}V
	\end{align*}
	%
Für das Quadrupolmoment $Q$ können anhand der Abbildung~\ref{fig:2013-05-17-00} folgende Überlegungen angestellt werden:
	%
	\begin{align*}
		Q &= \frac{1}{2}Q_{zz} \gtrless 0
		\intertext{falls V so, dass überwiegend}
		z^2 &\gtrless \frac{1}{2}(x^2 + y^2)
	\end{align*}
	%

	\begin{figure}[htpb]
		\centering
		\begin{pspicture}(-0.5,-2.5)(4,2.3)
			\psline[arrows=->](0,-2)(0,2)
			\uput[180](0,2){\color{DimGray} $z$}
			\psline[arrows=->](0,0)(2;60)
			\psarcn[arrows=->](0,0){1.8}{90}{60}
			\uput{1.5}[75](0,0){\color{DimGray} $\vartheta$}
			\psline(0.5,1)(0.5,-1)
			\psline(-0.5,1)(-0.5,-1)
			\psarc(0,1){0.5}{0}{180}
			\psarc(0,-1){0.5}{180}{0}
			\psellipticarc(0,0)(0.5,0.25){180}{0}
			\psellipticarc[linestyle=dotted,dotsep=1pt](0,0)(0.5,0.25){0}{180}
			\psellipticarc(0,1)(0.5,0.25){180}{0}
			\psellipticarc[linestyle=dotted,dotsep=1pt](0,1)(0.5,0.25){0}{180}
			\psellipticarc(0,-1)(0.5,0.25){180}{0}
			\psellipticarc[linestyle=dotted,dotsep=1pt](0,-1)(0.5,0.25){0}{180}
			\uput[-90](0,-2){\color{DimGray} $Q > 0$}

			\psline[arrows=->](3,-2)(3,2)
			\uput[180](0,2){\color{DimGray} $z$}
			\psellipse(3,0)(1,0.5)
			\psellipticarc(3,0)(1,0.25){180}{0}
			\psellipticarc[linestyle=dotted,dotsep=1pt](3,0)(1,0.25){0}{180}
			\uput[-90](3,-2){\color{DimGray} $Q < 0$}
		\end{pspicture}
		\caption{Rotationsinvariante Ladungsverteilungen.}
		\label{fig:2013-05-17-00}
	\end{figure}
\end{example}

\begin{example}
	Gestreckter Quadrupol:
	%
	\begin{figure}[htpb]
		\centering
		\begin{pspicture}(-2.5,-2.5)(2.5,2.5)
			\psaxes[xAxis=false,labels=none,ticks=none,arrows=->](0,0)(-2.5,-2.5)(2.5,2.5)[,0][{\color{DimGray} $z$},0]
			\psline(2.5;35.3)(-2.5;35.3)
			\psline(2.5;144.7)(-2.5;144.7)
			\psarcn[linecolor=DarkOrange3,arrows=->](0,0){2.2}{90}{35.3}
			\uput{2.5}[62.65](0,0){\color{DarkOrange3} \SI{54.7}{\degree}}
			\psplot[linecolor=MidnightBlue,plotpoints=200,polarplot=true]{0}{\psPiTwo}{1.0 * 1/2*(3*cos(x+\pstPI2)^2 - 1)}
			\psplot[linecolor=MidnightBlue,plotpoints=200,polarplot=true]{0}{\psPiTwo}{1.5 * 1/2*(3*cos(x+\pstPI2)^2 - 1)}
			\psplot[linecolor=MidnightBlue,plotpoints=200,polarplot=true]{0}{\psPiTwo}{2.0 * 1/2*(3*cos(x+\pstPI2)^2 - 1)}
		\end{pspicture}
		\caption{Fernfeld eines gestreckten Quadrupols.}
	\end{figure}
	%
	\begin{align*}
		\phi_3(\bm{r}) &= \frac{1}{2}Q_{\alpha\beta}\frac{x_{\alpha}x_{\beta}}{r^5} = \frac{1}{2r^5}\sum\limits_{\alpha}Q_{\alpha\alpha}x^2_{\alpha}\\
		&= \frac{Q}{2r^5}\left[-x^2 - y^2 + 2z^2\right]\\
		&= \frac{Q}{2r^5}\left[3z^2 - r^2\right]\\
		&= \frac{Q}{2r^3}\left[3\cos^2 \vartheta - 1\right]
	\end{align*}
	%
	Damit verschwindet der Zähler für $\cos^2 \vartheta = 1/3 \implies \vartheta_0 = \SI{54.7}{\degree}, \SI{125.3}{\degree}$. Wir betrachten hier den Fall, dass $\phi_3 = \text{const}$ ist, d.h., wenn der Zähler gegen Null geht muss auch der Nenner gegen Null gehen. Dies erklärt das Einschnüren der oben abgebildeten Äquipotentiallinien. Denn für diese Winkel muss $r\to 0$ gehen, um $\phi = \text{const}$ zu erreichen.
\end{example}

\begin{notice}[Zusammenfassung:]
	\begin{align*}
		\phi(\bm{r}) &= \underbrace{\underbrace{\frac{q_{\text{tot}}}{r}}_{\text{Mono-}}}_{\sim\frac{1}{r}} + \underbrace{\underbrace{\frac{\bm{p}\cdot\bm{r}}{r^3}}_{\text{Di-}}}_{\sim\frac{1}{r^2}} +\underbrace{ \underbrace{\frac{1}{6}Q_{\alpha\beta}\frac{3x_{\alpha}x_{\beta} - r^2\delta_{\alpha\beta}}{r^5}}_{\text{Quadrupol}}}_{\sim\frac{1}{r^3}} + \ldots\\
	\end{align*}	
\end{notice}

\paragraph{Weitere Formen der Multipolentwicklung:}
%
Wir betrachten Abbildung~\ref{fig:2013-05-17-1}, was der Konstruktion der Kugelkoordinaten entspricht. Für den Betrag des Abstandes der Vektoren $\bm{r}$ und $\bm{r}'$ gilt:
\[
  |\bm{r} - \bm{r}'| = \sqrt{r^2 + r'^2 - 2rr'\cos\gamma}\,,\qquad\text{mit}\,\cos\gamma = \frac{\bm{r}\cdot\bm{r}'}{rr'}
\]
Wir führen die Definitionen
%
\begin{align*}
	r_> &\coloneq \mathrm{max}(r,r')\\
	r_< &\coloneq \mathrm{min}(r,r')
\end{align*}
%
ein.

\begin{figure}[htpb]
	\centering
	\begin{pspicture}[coorType=2](-1.5,-1.5)(3,3.5)
		\pstThreeDCoor[
			linecolor=DimGray,
			xMax=2,yMax=2.5,zMax=3,
			xMin=0,yMin=0,zMin=0,
			nameX={\color{DimGray} $x$},nameY={\color{DimGray} $y$},nameZ={\color{DimGray} $z$}
		]
		\pstThreeDNode(0,0,0){O}
		\pstThreeDNode(0,0,1){U}
		\pstThreeDNode(0,0,-1){D}
		\pstThreeDLine[linecolor=MidnightBlue,arrows=->](0,0,0)(1.5,1.5,2.5)
		\pstThreeDLine[linecolor=DarkOrange3,arrows=->](0,0,0)(0.5,2.5,2.5)
		\pstThreeDLine[arrows=->](0,0,0)(1.5,1.5,0)
		\pstThreeDLine[arrows=->](0,0,0)(0.5,2.5,0)
		\pstThreeDLine[linestyle=dotted,dotsep=1pt](1.5,1.5,2.5)(1.5,1.5,0)
		\pstThreeDLine[linestyle=dotted,dotsep=1pt](0.5,2.5,2.5)(0.5,2.5,0)
		\pstThreeDNode(0.5,2.5,2.5){r}
		\pstThreeDNode(1,0,0){phi11}
		\pstThreeDNode(1.1,0,0){phi21}
		\pstThreeDNode(0.75,0.75,0){phi12}
		\pstThreeDNode(0.27,1.27,0){phi22}
		\ncarc[linecolor=MidnightBlue,arrows=->,arcangle=-15]{phi11}{phi12}
		\ncarc[linecolor=DarkOrange3,arrows=->,arcangle=-20]{phi21}{phi22}
		\pstThreeDNode(0,0,1){theta11}
		\pstThreeDNode(0.75,0.75,1.25){theta12}
		\pstThreeDNode(0,0,2.5){theta21}
		\pstThreeDNode(0.4,2,2){theta22}
		\ncarc[linecolor=MidnightBlue,arrows=->,arcangle=20]{theta11}{theta12}
		\ncarc[linecolor=DarkOrange3,arrows=->,arcangle=20]{theta21}{theta22}
		\pstThreeDNode(1.5,1.5,2.5){gamma1}
		\pstThreeDNode(0.3,1.5,1.5){gamma2}
		\ncarc[linecolor=Purple,arrows=->,arcangle=20]{gamma1}{gamma2}
		\uput[150](gamma1){\color{MidnightBlue} $\bm{r}'$}
		\uput[50](r){\color{DarkOrange3} $\bm{r}$}
		\uput{0.2}[70]([nodesep=0.7]{theta21}theta22){\color{DarkOrange3} $\vartheta$}
		\uput[90]([nodesep=0.1]{theta11}theta12){\color{MidnightBlue} $\vartheta'$}
		\uput[160](phi12){\color{MidnightBlue} $\varphi'$}
		\uput[-90](phi22){\color{DarkOrange3} $\varphi$}
		\uput[190](gamma2){\color{Purple} $\gamma$}
	\end{pspicture}
	\caption{Zur Multipolentwicklung in Kugelkoordinaten.}
	\label{fig:2013-05-17-1}
\end{figure}

Für die Multipolentwicklung müssen wir $\frac{1}{|\bm{r} - \bm{r}'|}$ betrachten. Mit den obigen Definitionen erhalten wir damit:
%
\begin{enumerate}
	\item
		\begin{itemalign}
			\frac{1}{|\bm{r} - \bm{r}'|} &= \frac{1}{r_>}\frac{1}{\sqrt{1 + \left(\frac{r_<}{r_>}\right)^2 - 2\frac{r_<}{r_>}\cos\gamma}}\\
			&= \frac{1}{r_>}\sum\limits_{\ell = 0}^{\infty}\left(\frac{r_<}{r_>}\right)^{\ell}P_{\ell}(\cos\gamma)
		\end{itemalign}
	%
		Dabei sind $P_{\ell}(\cos\gamma)$ die \acct{Legendre-Polynome} $\ell$-ten Grades in $\cos\gamma$.
	\item
		Für den allgemeinen Fall gilt es $\cos\gamma$ durch $\vartheta$, $\varphi$ und $\vartheta'$, $\varphi'$ auszudrücken. Wird dies gemacht, so erhalten wir:
		\begin{align*}
			\boxed{
			\frac{1}{|\bm{r}-\bm{r}'|} = \frac{4\pi}{r_>}\sum\limits_{\ell = 0}^{\infty}\sum\limits_{m = -\ell}^{\ell}\frac{1}{2\ell + 1}\left(\frac{r_<}{r_>}\right)^{\ell}Y_{\ell,m}(\vartheta,\varphi)Y^*_{\ell,m}(\vartheta',\varphi')
			}
		\end{align*}
		Dabei gibt $Y_{\ell,m}(\vartheta,\varphi)$ Informationen über $r$ und $Y^*_{\ell,m}(\vartheta',\varphi')$ Informationen über $r'$. Damit erhalten wir die Multipolentwicklung in Kugelkoordinaten:
		%
		\begin{align*}
			\phi(\bm{r}) &= \int\frac{\varrho(\bm{r}')}{|\bm{r} - \bm{r}'|}\,\mathrm{d}V'\\
			&= \sum\limits_{\ell = 0}^{\infty}\sum\limits_{m = -\ell}^{\ell}\frac{4\pi}{2\ell + 1}Y_{\ell,m}(\vartheta,\varphi)\int_{V}\frac{r_<^{\ell}}{r_>^{\ell + 1}}Y^*_{\ell, m}(\vartheta',\varphi')\varrho(\bm{r}')\,\mathrm{d}V'
		\end{align*}
		%
		Dies ist der allgemeine Fall einer Multipolentwicklung, womit auch innerhalb des Bereichs der Ladungsverteilung das entsprechende Potential (Mono-, Di-, Quadrupol) berechnet werden kann. 
		
		Im Fernfeld ($\bm{r}\notin V$):
		%
		\begin{align*}
			\phi(\bm{r}) &= \sum\limits_{\ell = 0}^{\infty}\sum\limits_{m= -\ell}^{\ell}\frac{4\pi}{2\ell + 1}\frac{Y_{\ell,m}(\vartheta,\varphi)}{r^{\ell + 1}}\underbrace{\int_{V}r'^{\ell}Y^*_{\ell,m}(\vartheta',\varphi')\varrho(\bm{r}')\,\mathrm{d}V'}_{\text{Multipolmomente}:\, q_{\ell m}}
		\end{align*}
		%
		Hierbei gilt:
		%
		\begin{align*}
			\ell &= 0\;:\text{Monopol}\\
			\ell &= 1\;:\text{Dipol}\\
			\ell &= 2\;:\text{Quadrupol}
		\end{align*}
		%
		zu jedem $\ell$ gibt es $2\ell + 1$ unabhängige Komponenten.
\end{enumerate}
%
\subsection{Vektorpotential \texorpdfstring{$\bm{A}$}{A}: Magnetostatik}
%
Im Fall der Magnetostatik haben ist das Potential keine skalare Funktion $\phi(\bm{r})$ mehr. Vielmehr liegt ein Vektorfeld $\bm{A}(\bm{r})$, dass sogenannte \acct*{Vektorpotential} vor. Hierfür gilt:
%
\begin{align*}
	\bm{A}(\bm{r}) &= \frac{1}{c}\int_{\mathbb{R}^3}\frac{\bm{j}(\bm{r}')}{|\bm{r} - \bm{r}'|}\,\mathrm{d}V'
\intertext{wobei man aus der Taylorentwicklung}
	\frac{1}{|\bm{r} - \bm{r}'|} &= \frac{1}{r} + \frac{\bm{r}\cdot\bm{r}'}{r^3} + \ldots
\end{align*}
%
erhält. In den Übungen wurde gezeigt, dass sich aus der entsprechenden Multipolentwicklung Folgendes ergibt:
%
\begin{align*}
	\bm{A}_1 &= \bm{0}\\
	\bm{A}_2 &= \frac{\bm{m}\times\bm{r}}{r^3}\;\text{(magnetisches Dipolpotential)}\\
	\bm{m} &= \frac{1}{2c}\int\bm{r}\times\bm{j}(\bm{r}')\,\mathrm{d}V'\;\text{(magn. Dipolmoment)}\\
	\bm{B}_2(\bm{r}) &= \frac{1}{r^5}\left[3(\bm{m}\cdot\bm{r})\bm{r} - \bm{m}r^2\right]\;\text{(magn. Dipolfeld)}\\
	\bm{m} &= \frac{I F}{c}\bm{n}
\end{align*}
%

\begin{notice}[Vergleich:] Elektrischer Dipol versus magnetischer Dipol.

	\begin{minipage}[t]{0.45\textwidth}
		%
		\begin{center}
			\begin{pspicture}(-2,-2)(2,2)
				\psline[linestyle=dotted,dotsep=1pt](0,-2)(0,2)
				\psdots*(0,1)(0,-1)
				\uput[0](0,1){\color{DimGray} $+$}
				\uput[0](0,-1){\color{DimGray} $-$}
				\pscurve[arrows=->](0,1)(-1,2)(-1,-2)(0,-1)
				\pscurve[arrows=->](0,1)(-0.7,1.5)(-0.7,-1.5)(0,-1)
				\pscurve[arrows=->](0,1)(-0.7,0)(0,-1)
				\pscurve[arrows=->](0,1)(0.7,0)(0,-1)
				\pscurve[arrows=->](0,1)(0.7,1.5)(0.7,-1.5)(0,-1)
				\pscurve[arrows=->](0,1)(1,2)(1,-2)(0,-1)
			\end{pspicture}
		\end{center}
		%
		\begin{align*}
			\bm{E}_2 &= \frac{1}{r^5}\left[3(\bm{p}\cdot\bm{r})\bm{r} - \bm{p}r^2\right]
		\end{align*}
		%
		Ladungen: Quellen, keine Wirbel
	\end{minipage}
	\hspace{0.1\textwidth}
	\begin{minipage}[t]{0.45\textwidth}
		\begin{center}
			\begin{pspicture}(-2,-2)(2,2)
				\psline[linestyle=dotted,dotsep=1pt](0,-2)(0,2)
				\psellipticarc[arrows=->](1,0)(0.25,0.5){0}{180}
				\psellipticarc[arrows=->](1,0)(0.25,0.5){180}{0}
				\psellipticarc[arrows=->](1,0)(0.5,1){0}{180}
				\psellipticarc[arrows=->](1,0)(0.5,1){180}{0}

				\pscircle(1,0){0.1}
				\pspolygon[linestyle=none,fillstyle=solid,fillcolor=white](-1,0.1)(1,0.1)(1,-0.1)(-1,-0.1)
				\pscircle(-1,0){0.1}
				\psline(-1,0.1)(1,0.1)
				\psline(-1,-0.1)(1,-0.1)
				\psellipticarc[arrows=<-](-1,0)(0.25,0.5){0}{180}
				\psellipticarc[arrows=<-](-1,0)(0.25,0.5){180}{0}
				\psellipticarc[arrows=<-](-1,0)(0.5,1){0}{180}
				\psellipticarc[arrows=<-](-1,0)(0.5,1){180}{0}
				\uput[-90](-1,0){\color{DimGray} $-$}
				\uput[-90](1,0){\color{DimGray} $+$}
			\end{pspicture}
		\end{center}
		%
		\begin{align*}
			\bm{B}_2 &= \frac{1}{r^5}\left[3(\bm{m}\cdot\bm{r})\bm{r} - \bm{m}r^2\right]
		\end{align*}
		%
		Strom: Wirbel, keine Quellen
	\end{minipage}
	%

	Insgesamt gilt: $\bm{p}$ und $\bm{m}$ sind punktförmig, was einen gleichen Feldverlauf zur Folge hat.
\end{notice}
