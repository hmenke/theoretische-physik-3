% Henri Menke, Michael Schmid, Jan Schnabel , Marcel Klett 2013 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

% Vorlesung am 08.05.2013
% TeX: Michi

\renewcommand{\printfile}{2013-05-08}

\begin{notice}[Wiederholung:]
	Die Maxwellgleichungen in differentieller Form lauten:
	%
	\begin{align*}
		\div \bm{E} &= 4 \pi \varrho  & \rot \bm{E} &= - \frac{1}{c} \partial_t \bm{B} \\
		\div \bm{B} &= 0 & \rot \bm{B} &= - \frac{4 \pi}{c} \bm{j} + \frac{1}{c} \partial_t \bm{E}
	\end{align*}
	% 
	Der Energieerhaltungssatz der Elektrodynamik in differentialer Form lautet:
	\[
		- \partial_t u_{\text{Feld}} = \partial_t W_{\text{mech}} + \div \bm{S}
	\]
	in integraler Form lautet er:
	\[
		\underbrace{- \frac{\mathrm{d}}{\mathrm{d}t} \int_{V} u_{\text{Feld}} \; \mathrm{d}V}_{{I}_1} = \underbrace{\frac{\mathrm{d}}{\mathrm{d}t} \int_{V} W_{\text{mech}} \, \mathrm{d}V}_{{I}_2} + \underbrace{\int_{\partial V} \bm{S} \cdot \mathrm{d}\bm{f}}_{{I}_3}.
	\]
	Hierbei entspricht Integral ${I}_1$ der Abnahme der Feldenergie im Volumen $V$ pro Zeit, Integral ${I}_2$ der Zunahme der mechanischen Energie im Volumen $V$ pro Zeit und Integral ${I}_3$ der ausströmenden Feldenergie pro Zeit.
\end{notice}

\minisec{Exkurs: realer Leiter, Ohmsches Gesetz}

Im folgenden betrachten wir einen Leiter der Länge $\ell$ durch den ein stationärer Strom $I$ fließt:

\begin{figure}[htpb]
	\centering
	\begin{pspicture}(-0.25,-1.0)(5,1.0)
		\psline(0,0.5)(4,0.5)
		\psline(0,-0.5)(4,-0.5)
		\psellipticarc(0,0)(0.25,0.5){90}{-90}
		\psellipse[fillstyle=hlines](4,0)(0.25,0.5)
		\psbrace[
			fillcolor=DimGray,
			braceWidth=0.5pt,
			braceWidthInner=4pt,
			braceWidthOuter=2pt,
			ref=lC,
			rot=90,
			nodesepB=5pt
		](0,-0.5)(4,-0.5){\color{DimGray} \clap{$\ell$}}
		\pcarc(4,0)(4.5,0.7)
		\uput[0](4.5,0.7){\color{DimGray} $F$}
		\psline[linecolor=MidnightBlue,arrows=->](0.5,0)(1,0)
		\uput[0](1,0){\color{MidnightBlue} stationär}
		\psline[linecolor=DarkOrange3,arrows=->](1,0.7)(3,0.7)
		\uput[90](2,0.7){\color{DarkOrange3} $\bm{E} = \mathrm{const}$}
		\rput(0,-0.5){
			\psxTick(0){\color{DimGray} P_1 : \phi_1}
			\psxTick(4){\color{DimGray} P_2 : \phi_2}
		}
	\end{pspicture}
	\caption{Ein realer Leiter mit der Querschnittsfläche $F$, durch den ein stationärer Strom fließt.}
\end{figure}

Im Leiter gilt:
%
\begin{align*}
	\partial_t \varrho &= 0 \implies \div \bm{j} = 0 &  \text{\color{DimGray}{d.h.\ stationär}} \\
	\partial_t \bm{B} &= 0 \implies \rot \bm{E} = 0 \\
	\implies \bm{E} &= - \nabla \phi 
\end{align*}
%
Hierbei ist das Minuszeichen vor dem Gradient Konvention und $\phi$ bezeichnet das Potential. An den beiden Enden des Leiters liegt ein unterschiedliches Potential vor, die Potentialdifferenz bezeichnet man allgemein hin als Spannung $U$. Es gilt also:
%
\begin{align*}
	\underbrace{\int_{P_1}^{P_2} \bm{E} \cdot \mathrm{d}\bm{r}}_{= E\cdot \ell} &= - \int_{P_1}^{P_2} \left(\nabla \phi\right) \cdot \mathrm{d}\bm{r} = \underbrace{\phi_1 - \phi_2}_{= U}
\end{align*}
%
Experimente zeigen schließlich, das folgendes für die Stromdichte $\bm{j}$ gilt:
\[
	\boxed{\bm{j} = \sigma \bm{E}} \qquad \left(j_\alpha = \sigma_{\alpha\beta} E_\beta \right)
\]
$\sigma$ wird auch als Leitfähigkeit bezeichnet und ist allgemein hin ein Tensor. Damit gilt weiter:
%
\begin{align*}
	\int_{F} \bm{j} \cdot \mathrm{d}\bm{f} &= I = \sigma \int_{F} \bm{E} \cdot \mathrm{d}\bm{f} \\
	&= \sigma \cdot E \cdot D = \frac{\sigma F}{\ell} \left(\phi_1 - \phi_2\right)
\end{align*}
%
Damit folgt das \acct{Ohmsche Gesetz}:
\[
	\boxed{\phi_1 - \phi_2 \equiv U = I R_{12} \quad, \quad R_{12} = \frac{\ell}{\sigma F}}
\]

\begin{notice}
	$\sigma < \infty$ aufgrund der Stöße der Elektronen mit thermischen Metallgitterschwingungen (Phononen) und Gitterdefekte.
\end{notice}
%
\begin{align*}
	\bm{j} \cdot \bm{E} &= \sigma E^2 & {\color{DimGray} (= \sigma_{\alpha\beta} E_\alpha E_\beta)} \\
	&= \partial_t W_{\text{mech}} = \frac{\ell}{R\cdot F} \left(\frac{U}{\ell}\right)^2 \\
	&= \frac{U^2}{R} \frac{\ell}{(F \cdot \ell) = V} 
\end{align*}
%
Somit ergibt sich:
\[
	\boxed{\bm{j}\cdot\bm{E} = \sigma E^2}
\]
Dies bezeichnet auch die pro Zeit und Volumen erzeugte joulsche Wärme (dissipative mechanische Energie)
\[
	w = \partial W_{\text{mech}} \cdot V \cdot \Delta t = U \cdot I \cdot \Delta t.
\]

\begin{notice}[Frage:]
	Wie kommt im Feldbild die elektrische Energie zum Verbraucher?
\end{notice}

\begin{figure}[htpb]
	\centering
	\begin{pspicture}(-3.5,-3.5)(3.5,0.5)
		\pstThreeDLine[arrows=>-](4,0,0)(0,0,0)(0,1.5,0)
		\pstThreeDNode(4.5,0,0){N1}
		\pstThreeDNode(4.5,4,0){N2}
		\pstThreeDLine[arrows=->](0,2.5,0)(0,4,0)(4,4,0)
		\pstThreeDLine[doubleline=true,arrows=->](0,2,0)(-2,2,0)\pstThreeDNode(-2,2,0){Wmech}
		\pstThreeDCircle(0,2,0)(0,0.5,0)(0,0,0.5)
		\pstThreeDLine(0,1,0)(0,1,0.7)
		\pstThreeDLine(0,3,0)(0,3,0.7)
		\pstThreeDLine[arrows=<->](0,1,0.6)(0,3,0.6)\pstThreeDNode(0,2,0.6){phi}

		\pstThreeDCircle[linecolor=Purple](2,0,0)(0,0.5,0)(0,0,0.5)
		\pstThreeDLine[linecolor=Purple,arrows=->](2,0.5,0)(2,0.5,-0.5)
		\pstThreeDLine[linecolor=Purple,arrows=->](2,-0.5,0)(2,-0.5,0.5)
		\pstThreeDLine[linecolor=Purple,arrows=->](2,0,0.5)(2,0.5,0.5)
		\pstThreeDLine[linecolor=Purple,arrows=->](2,0,-0.5)(2,-0.5,-0.5)

		\pstThreeDLine[linecolor=MidnightBlue,arrows=->](2,0,0)(2,4,0)\pstThreeDNode(2,2,0){E}
		\pstThreeDLine[arrows=->](2,1,0)(1,1,0)\pstThreeDNode(1,1,0){S1}
		\pstThreeDLine[linecolor=DarkOrange3,arrows=->](2,1,0)(2,1,-1)\pstThreeDNode(2,1,-1){B1}
		\pstThreeDLine[arrows=->](2,3,0)(1,3,0)\pstThreeDNode(1,3,0){S2}
		\pstThreeDLine[linecolor=DarkOrange3,arrows=->](2,3,0)(2,3,-1)\pstThreeDNode(2,3,-1){B2}

		\uput[90](Wmech){\color{DimGray} $W_\mathrm{mech}$}
		\uput[50](phi){\color{DimGray} $\Delta \phi$}
		\uput[-90](E){\color{MidnightBlue} $\bm{E}$}
		\uput[140](S1){\color{DimGray} $\bm{s}$}
		\uput[140](B1){\color{DarkOrange3} $\bm{B}$}
		\uput[140](S2){\color{DimGray} $\bm{s}$}
		\uput[140](B2){\color{DarkOrange3} $\bm{B}$}
		\cput(N1){\color{DimGray} 1}
		\cput(N2){\color{DimGray} 2}
	\end{pspicture}
	\caption{Der Einfachheit halber wird der Aufbau als Supraleiter angenommen, d.h.\ es findet kein Spannungabfall entlang des Leiters statt.}
\end{figure}

Der Einfachheit halber als Supraleiter angenommen:
%
\begin{itemize}
	\item kein Spannungsabfall längs des Leiters
	\item Leiter (1) auf Potential (1) und Leiter (2) auf Potential (2) 
	\item $\bm{E}$ verläuft senkrecht von (1) nach (2)
\end{itemize}

\paragraph{Realer Leiter:}
$\bm{E}$ hat auch eine Komponente in Richtung von $I$, um den Strom aufrecht zu erhalten.

\begin{figure}[htpb]
	\centering
	\begin{pspicture}(-0.25,-2.5)(4.25,0.5)
		\psline(0,0.5)(4,0.5)
		\psline(0,-0.5)(4,-0.5)
		\psellipticarc(0,0)(0.25,0.5){90}{-90}
		\psellipse(4,0)(0.25,0.5)
		\psline[linecolor=MidnightBlue,arrows=->](0.5,0)(1.5,0)
		\uput[0](1.5,0){\color{MidnightBlue} $\bm{I}$}
		\rput(1,-0.8){\color{DarkOrange3} $\otimes$}
		\rput(2,-0.8){\color{DarkOrange3} $\otimes$}
		\rput(3,-0.8){\color{DarkOrange3} $\otimes$}
		\uput[0](2,-0.8){\color{DarkOrange3} $\bm{B}$}
		\rput(2.8,-2.2){
			\pnode(0,0){O}
			\psaxes[labels=none,ticks=none,arrows=->](0,0)(0,0)(1,1)[{\color{DimGray} $\bm{s}_\parallel$}, -90][{\color{DimGray} $\bm{s}_\perp$}, 180]
			\psline[linestyle=dotted,dotsep=1pt](1,0)(1,1)(0,1)
			\psline[arrows=->](0,0)(1,1)
			\uput[45](1,1){\color{DimGray} $\bm{s}$}
		}
		\pcline[arrows=->](1.5,-0.5)(O)
	\end{pspicture}
	\caption{$\bm{s}_\perp$ zeigt zum Leiter und $\bm{s}_\parallel$ zeigt zum Verbraucher.}
\end{figure}

$\bm{s}_\perp$ zeigt zu Leiter hin, d.h.\ die joulsche Wärme erfolgt durch Zufluss aus der Feldenergie

\subsection{Impulsbilanz}

Wir betrachten das Volumen $V$ aus Abbildung~\ref{fig:2013-05-08-1}.

\begin{figure}[htpb]
	\centering
	\begin{pspicture}(0,0)(4.5,2)
		\psccurve(0,1)(1,0.5)(2,0)(2.5,1)(2,1.5)(1,1.7)
		\rput(1,1){\color{DimGray} $V$}
		\pscurve[arrows=->](1.5,1)(1.7,1.1)(1.8,0.9)(2.3,1)
		\pcarc[arrows=->](3,1)(4,1.5)
		\pcarc[arrows=->](3,1.5)(3.5,0.5)
		\uput[0](4,1.5){\color{DimGray} $\bm{E}$}
		\uput[0](3.5,0.5){\color{DimGray} $\bm{B}$}
		\pcarc(1.5,0.5)(1,0.2)
		\uput[180](1,0.2){\color{DimGray} $\varrho,\bm{j}$}
	\end{pspicture}
	\caption{Innerhalb des Volumen $V$ befinden sich Ladungen mit der Ladungsdichte $\varrho$ und der Stromdichte $\bm{j}$. Auf diese wirken die externen Felder $\bm{E}$ und $\bm{B}$ ein.}
	\label{fig:2013-05-08-1}
\end{figure}

Die Gesamtkraft auf $\varrho$, $\bm{j}$ in $V$:
\[
	\int_{V} \bm{K} \, \mathrm{d}V = \frac{\mathrm{d}}{\mathrm{d}t} \bm{P}_{\text{mech}}
\]
Somit gilt für $\bm{K}$:
%
\begin{align*}
	\bm{K} &= \varrho \bm{E} + \frac{1}{c} \bm{j} \times \bm{B} \\
\intertext{mit $\varrho = \tfrac{1}{4 \pi} \nabla \bm{E}$ und $ \bm{j} = \tfrac{c}{4 \pi} \nabla \times \bm{B} - \tfrac{1}{4 \pi} \partial_t \bm{E}$ gilt:}
	&= \frac{1}{4 \pi} \bm{E} \left(\nabla \cdot \bm{E}\right) + \frac{1}{4 \pi} \left(\nabla \times  \bm{B}\right) \times \bm{B} - \frac{1}{4 \pi c} \left(\partial_t\bm{E}\right) \times \bm{B}
\intertext{mit $ \left(\partial_t\bm{E}\right) \times \bm{B} = \partial_t(\bm{E} \times \bm{B}) - \bm{E} \times \partial_t \bm{B}  $ und $\partial\bm{B} = - c \nabla \times \bm{E}$ folgt:}
	&= \frac{1}{4 \pi} \bm{E} \cdot \left(\nabla \cdot \bm{E}\right) - \frac{1}{4 \pi} \bm{B} \times \left(\nabla \times \bm{B} \right) - \frac{1}{4 \pi c} \partial_t \left(\bm{E} \times \bm{B}\right) - \frac{1}{4 \pi} \bm{E} \times \left(\nabla \times \bm{E}\right) \\
	&= \!
	\begin{multlined}[t]
		\frac{1}{4 \pi} \bm{E} \cdot \left(\nabla \cdot \bm{E}\right)- \frac{1}{4 \pi} \bm{E} \times \left(\nabla \times \bm{E}\right) + \frac{1}{4 \pi} \bm{B} \left(\nabla \cdot \bm{B}\right) \\
		- \frac{1}{4 \pi} \bm{B} \times \left(\nabla \times \bm{B}\right) - \frac{1}{4 \pi c} \partial_t \left(\bm{E} \times \bm{B}\right) 		
	\end{multlined}
\end{align*}
%
\begin{notice}[Nebenrechnung:]
%
\begin{align*}
	E_\alpha (\nabla \cdot \bm{E}) - \left[\bm{E} \times \left(\nabla \times \bm{E}\right)\right]_\alpha &= E_\alpha \partial_\beta E_\beta - \varepsilon_{\alpha \beta \gamma} E_\beta \varepsilon_{\gamma \mu \nu} \partial_\mu E_\nu \\
	&= E_\alpha \partial_\beta E_\beta - \varepsilon_{\alpha \beta \gamma} \varepsilon_{\gamma \mu \nu} E_\beta \partial_\mu E_\nu \\
	&= E_\alpha \partial_\beta E_\beta - \left[ \delta_{\alpha \mu} \delta_{\beta \nu} - \delta_{\alpha \nu} \delta_{\beta \mu} \right] E_\beta \partial_\mu E_\nu \\
	&=  E_\alpha \partial_\beta E_\beta - E_\beta \partial_\alpha E_\beta + E_\beta \partial_\beta E_\alpha \\
	&= \partial_\beta \left(E_\alpha E_\beta\right) - \frac{1}{2} \partial_\alpha \left(E_\beta E_\beta\right) \\
	&= \partial_\beta \left[E_\alpha E_\beta - \frac{1}{2} \delta_{\alpha \beta} \bm{E}^2\right]
\end{align*}
%
\end{notice}
%

Somit lässt sich die \acct{Impulsdichte des elektromagnetischen Feldes} definieren:
\[
	\boxed{\bm{p}_{\text{Feld}} \coloneq \frac{1}{4 \pi  c} \bm{E} \times \bm{B} = \frac{1}{c^2} \bm{S}}
\]
Der \acct*{Maxwellsche Spannungstensor}\index{Maxwellscher Spannungstensor} ist hierbei wie folgt definiert:
\[
	\boxed{ \tensor{T}_{\alpha\beta} \coloneq \frac{1}{4 \pi} \left(E_\alpha E_\beta + B_\alpha B_\beta \right) - \delta_{\alpha \beta} u_{\text{Feld}}  = \tensor{T}_{\beta \alpha}}
\]
Es zeigt sich, dass der Tensor symmetrisch ist. Somit folgt:
%
\begin{align*}
	\frac{\mathrm{d}}{\mathrm{d}t} \left(\bm{P}_{\text{mech}}\right)_\alpha &= \int_{V} \partial_\beta \tensor{T}_{\alpha \beta} \, \mathrm{d}V - \frac{\mathrm{d}}{\mathrm{d}t} \int_{V} p_\alpha^{\text{Feld}} \, \mathrm{d}V 
\intertext{mit $\bm{P}_{\text{Feld}} = \int_{V} \bm{p}_{\text{Feld}} \, \mathrm{d}V$ folgt:}
	\frac{\mathrm{d}}{\mathrm{d}t} \underbrace{\left(P_\alpha^{\text{mech}} + P_\alpha^{\text{Feld}}\right)}_{P_\alpha^{\text{tot}}} &= \int_{V} \partial_\beta \tensor{T}_{\alpha \beta} \; \mathrm{d}V \stackrel{\text{Gauß}}{=} \int_{\partial V} \tensor{T}_{\alpha \beta} n_\beta \; \mathrm{d}f
\end{align*}
%
Der Term:
\[
	\tensor{T}_{\alpha \beta} = \left(\tensor{T} \bm{n}\right)_\alpha
\]
entspricht der Kraft pro Fläche in $\alpha$-Richtung auf ein Oberflächenelement mit Normalen $\bm{n}$. 

Die Drehimpulsdichte $\bm{\ell}_{\text{Feld}}$ des elektromagnetischen Feldes ist wie folgt definiert:
\[
	\bm{\ell}_{\text{Feld}} \coloneq \bm{r} \times \bm{p}_{\text{Feld}}.
\]
