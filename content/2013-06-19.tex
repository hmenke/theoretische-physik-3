% Henri Menke, Michael Schmid, Marcel Klett, Jan Schnabel 2013 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

% Vorlesung am 19.06.2013
% TeX: Henri 

\renewcommand{\printfile}{2013-06-19}

\paragraph{Lösung der inhomogenen Wellengleichung} Wir lösen die inhomogene Wellengleichung für das Vektorpotential $\bm{A}$ bzw.\ das skalare Potential $\phi$. Dabei gilt
%
\begin{align*}
	\quabla \psi(\bm{r},t) = - 4 \pi f(\bm{r},t)
\end{align*}
%
wobei $f(\bm{r},t)$ gegeben sei.

Wir betrachten Greens Funktion im $\mathbb{R}^4$:
%
\begin{align*}
	\boxed{\quabla G(\bm{r}-\bm{r}',t-t') = - 4 \pi \delta(\bm{r}-\bm{r}') \delta(t-t')}
\end{align*}

Mit der Kenntnis der Greenschen Funktion lautet die Lösung der inhomogenen Wellengleichung
%
\begin{align*}
	\boxed{\psi(\bm{r},t) = \psi_h(\bm{r},t) + \int_{\mathbb{R}^4} G(\bm{r}-\bm{r}',t-t') f(\bm{r},t) \, \mathrm{d}^3 r' \mathrm{d}t'}
\end{align*}
%
wobei $\psi_h$ die allgmeine Lösung der homogenen Wellengleichung $\quabla \psi_h = 0$ ist.

Die Berechnung erfolgt durch Fouriertransformation. Seien dabei $\bm{r}' = 0, t' = 0$:
%
\begin{align*}
	G(\bm{r},t) &= \int \frac{\mathrm{d}^3k}{(2\pi)^3} \frac{\mathrm{d}\omega}{2\pi} \, \tilde{G}(\bm{k},\omega) \, \mathrm{e}^{\mathrm{i}(\bm{k} \cdot \bm{r} - \omega t)} \\
	\delta(\bm{r})\delta(t) &= \int \frac{\mathrm{d}^3k}{(2\pi)^3} \frac{\mathrm{d}\omega}{2\pi} \, 1 \cdot \mathrm{e}^{\mathrm{i}(\bm{k} \cdot \bm{r} - \omega t)} \\
	\implies \left( -k^2 + \frac{\omega^2}{c^2} \right) \tilde{G}(\bm{k},\omega) &= - 4 \pi \\
	\implies \tilde{G} &= \frac{4 \pi c^2}{c^2 k^2 - \omega^2} + \tilde{G}_h
\end{align*}
%
mit $\tilde{G}_h = \Gamma(\bm{k}) \delta(\omega^2 - c^2 k^2)$, denn
%
\begin{align*}
	\left( -k^2+\frac{\omega^2}{c^2} \right) \Gamma(\bm{k}) \delta(\omega^2 - c^2 k^2) = 0
\end{align*}

Zur Erinnerung \[ \delta(g(\omega)) = \sum_{i} \frac{1}{|g'(\omega_i)|} \delta(\omega-\omega_i) \; , \quad \omega_i : g(\omega_i) = 0 \; \text{einfache Nullstellen} \]
%
Rücktransformation ergibt
%
\begin{align*}
	G_h(\bm{r},t) &= \int \frac{\mathrm{d}^3k}{(2\pi)^3} \, \Gamma(\bm{k}) \, \mathrm{e}^{\mathrm{i} \bm{k} \cdot \bm{r}} \int \frac{\mathrm{d}\omega}{2\pi} \, \underbrace{\delta\left( (\omega-ck)(\omega+ck) \right)}_{\frac{1}{2ck}\{\delta(\omega-ck)+\delta(\omega+ck)\}} \, \mathrm{e}^{-\mathrm{i} \omega t} \\
	&= \int \frac{\mathrm{d}^3k}{(2\pi)^3} \frac{\Gamma(\bm{k})}{4\pi c k} \mathrm{e}^{\mathrm{i} (\bm{k} \cdot \bm{r} - c k t)}
	+ \int \frac{\mathrm{d}^3k}{(2\pi)^3} \frac{\Gamma(\bm{k})}{4\pi c k} \mathrm{e}^{\mathrm{i} (\bm{k} \cdot \bm{r} + c k t)} \\
	&= \int \frac{\mathrm{d}^3k}{(2\pi)^3} \frac{\Gamma(\bm{k})}{4\pi c k} \mathrm{e}^{\mathrm{i} (\bm{k} \cdot \bm{r} - c k t)}
	+ \left( \int \frac{\mathrm{d}^3k}{(2\pi)^3} \frac{\Gamma^*(-\bm{k})}{4\pi c k} \mathrm{e}^{\mathrm{i} (\bm{k} \cdot \bm{r} - c k t)} \right)^*
\intertext{wenn $\Gamma(\bm{k}) = \Gamma^*(-\bm{k})$}
	&= 2 \Re \int \frac{\mathrm{d}^3k}{(2\pi)^3} \frac{\Gamma(\bm{k})}{4\pi c k} \mathrm{e}^{\mathrm{i} (\bm{k} \cdot \bm{r} - c k t)}
\end{align*}

Als nächstes ist der inhomogene Anteil an der Reihe:
%
\begin{figure}
	\centering
	\begin{pspicture}(-2.1,-2.1)(3,2.3)
		\psaxes[ticks=none,labels=none,arrows=->](0,0)(-2,-2)(2,2)[{\color{DimGray} $\Re \omega$}, 0][{\color{DimGray} $\Im \omega$}, 0]
		\psline[linecolor=DarkOrange3](-1.8,0)(1.8,0)
		\psline[linecolor=DarkOrange3,arrows=->](-1.1,0)(-1,0)
		\psline[linecolor=DarkOrange3,arrows=->](0.9,0)(1,0)
		\psarc[linestyle=none,arrows=->](0,0){1.8}{0}{45}
		\psarc[linestyle=none,arrows=->](0,0){1.8}{0}{135}
		\psarcn[linestyle=none,arrows=->](0,0){1.8}{0}{-135}
		\psarcn[linestyle=none,arrows=->](0,0){1.8}{0}{-45}
		\psarc(0,0){1.8}{0}{360}
		\cnode*[linecolor=MidnightBlue](-1,-1){2pt}{A}
		\cnode*[linecolor=MidnightBlue](1,-1){2pt}{B}
		\uput[90](A){\color{MidnightBlue} $-ck-\mathrm{i}\varepsilon$}
		\uput[90](B){\color{MidnightBlue} $ck-\mathrm{i}\varepsilon$}
		\pcarc(1.9;-60)(2,-1.5)\uput[0](2,-1.5){$t>0$}
		\pcarc(1.9;60)(2,1.5)\uput[0](2,1.5){$t<0$}
		\psline(1.6,0)(2.3,0.5)(1.8;20)\uput[0](2.3,0.5){\color{DimGray} $\mathcal{C}_1$}
		\psline(1.6,0)(2.3,-0.5)(1.8;-20)\uput[0](2.3,-0.5){\color{DimGray} $\mathcal{C}_2$}
		\rput(-1.5,2){\color{DimGray} $\boxed{\varepsilon > 0}$}
	\end{pspicture}
	\caption{Veranschaulichung der Integration in der komplexen Ebene.}
\end{figure}
%
\begin{align*}
	\tilde{G} &= \frac{2 \pi c}{k} \left( \frac{1}{\omega + c k} - \frac{1}{\omega - c k} \right) \\
	\tilde{G} &= \lim\limits_{\varepsilon \to 0^+} \frac{2 \pi c}{k} \left[ \frac{1}{\omega - (-ck-\mathrm{i}\varepsilon)} - \frac{1}{\omega - (ck-\mathrm{i}\varepsilon)} \right] \\
	\hat{G}(\bm{k},t) &= \int_{-\infty}^{\infty} \frac{\mathrm{d}\omega}{2 \pi} \, \tilde{G}(\bm{k},\omega) \, \mathrm{e}^{\mathrm{i} \omega t}
\end{align*}
%
Dabei gilt
%
\begin{align*}
	- \mathrm{i} \omega t &= - \mathrm{i} \left( \Re \omega + \mathrm{i} \Im \omega \right) t = (\Im \omega) t - \mathrm{i} (\Re \omega) t \\
	|\mathrm{e}^{- \mathrm{i} \omega t}| &= \mathrm{e}^{(\Im \omega) t} \to 0 \; \text{für} \; (\Im \omega) t < 0
\end{align*}

Das heißt: Bei $t > 0$ für $\Im \omega < 0$ und bei $t < 0$ für $\Im \omega > 0$, woraus folgt, dass für $t > 0$ der Integrationsweg nach unten und für $t < 0$ nach oben geschlossen werden kann.
%
\begin{align*}
	\implies
	\hat{G}(k,t<0) &= \oint_{\mathcal{C}_1} \frac{\mathrm{d}\omega}{2 \pi} \, \tilde{G}(\bm{k},\omega) \, \mathrm{e}^{-\mathrm{i} \omega t} \\
	\hat{G}(k,t>0) &= \oint_{\mathcal{C}_2} \frac{\mathrm{d}\omega}{2 \pi} \, \tilde{G}(\bm{k},\omega) \, \mathrm{e}^{-\mathrm{i} \omega t}
\end{align*}

\begin{theorem}[Residuensatz]\index{Residuensatz}
	Eine in einem Gebiet $\mathcal{G}\setminus\{z_0\}$ analytische Funktion lässt sich in eine Laurent-Reihe entwickeln:
	%
	\begin{align*}
		f(z) &= \sum_{n \in \mathbb{Z}} a_n (z-z_0)^n
	\end{align*}
	%
	wobei
	%
	\begin{align*}
		a_n &= \frac{1}{2 \pi \mathrm{i}} \oint (\zeta - z_0)^{-n-1} f(\zeta) \, \mathrm{d}\zeta \\
		a_{-1} &= \Res f(z) |_{z=z_0} = \frac{1}{2 \pi \mathrm{i}} \oint f(\zeta) \, \mathrm{d}\zeta
	\end{align*}
\end{theorem}

\begin{example}
	Sei $f(z) = \varphi(z)/\psi(z)$, sodass $\varphi$ und $\psi$ analytisch in $z_0$ sind, wobei $\psi(z_0) = 0$ eine einfache Nullstelle sei.
	%
	\begin{align*}
		\Res f(z_0) |_{z=z_0}
		&= \Res \left[ \frac{\varphi(z_0) + \varphi'(z_0)(z-z_0) + \dots}{\psi'(z_0)(z-z_0) + \dots} \right]_{z=z_0} \\
		&= \Res \left[ \frac{\varphi(z_0)}{\psi'(z_0)} \frac{1}{z-z_0} \right]_{z=z_0} \\
		&= \frac{\varphi(z_0)}{\psi'(z_0)}
	\end{align*}
	%
	Liegen mehrere Residuen vor, so wird darüber summiert
	%
	\begin{align*}
		\oint_{\mathcal{C}} f(z) \, \mathrm{d}z &= 2 \pi \mathrm{i} \sum_{i} \Res f(z) |_{z=z_i}
	\end{align*}
	%
	wobei $z_i$ alle singulären Punte sind, die von $\mathcal{C}$ umschlossen werden.
\end{example}

Für $\varepsilon > 0$ umschließt $\mathcal{C}_1$ keine Polstelle, also ist $\hat{G}(\bm{k},t<0) = 0$.
%
\begin{align*}
	\hat{G}(\bm{k},t>0)
	&= - 2 \pi \mathrm{i} \left\{
		\Res \left[ \frac{1}{2 \pi} \tilde{G}(\bm{k},\omega) \, \mathrm{e}^{-\mathrm{i} \omega t} \right]_{\omega=-ck-\mathrm{i}\varepsilon}
		+ \Res \left[ \frac{1}{2 \pi} \tilde{G}(\bm{k},\omega) \, \mathrm{e}^{-\mathrm{i} \omega t} \right]_{\omega=ck-\mathrm{i}\varepsilon}
	\right\}	
\end{align*}
%
Das Minuszeichen wurde eingeführt, um der Orientierung von $\mathcal{C}$ Rechnung zu tragen.
%
\begin{align*}
	\Res \left[ \frac{1}{2 \pi} \tilde{G}(\bm{k},\omega) \, \mathrm{e}^{-\mathrm{i} \omega t} \right]_{\omega=-ck-\mathrm{i}\varepsilon}
	&= \frac{c}{k} \mathrm{e}^{-\mathrm{i} (-ck-\mathrm{i}\varepsilon) t} \xrightarrow{\varepsilon \to 0} \frac{c}{k} \mathrm{e}^{\mathrm{i}ckt}
	\\
	\Res \left[ \frac{1}{2 \pi} \tilde{G}(\bm{k},\omega) \, \mathrm{e}^{-\mathrm{i} \omega t} \right]_{\omega=ck-\mathrm{i}\varepsilon}
	&= - \frac{c}{k} \mathrm{e}^{-\mathrm{i} (ck-\mathrm{i}\varepsilon) t} \xrightarrow{\varepsilon \to 0} - \frac{c}{k} \mathrm{e}^{-\mathrm{i}ckt}
\end{align*}

Also erhalten wir
%
\begin{align*}
	\hat{G}(\bm{k},t>0)
	&= - 2 \pi \mathrm{i} \left\{ \frac{c}{k} \mathrm{e}^{\mathrm{i}ckt} - \frac{c}{k} \mathrm{e}^{-\mathrm{i}ckt} \right\} \\
	&= - 2 \pi \mathrm{i} \frac{c}{k} 2 \mathrm{i} \sin ckt \\
	&= \frac{4 \pi c}{k} \sin ckt \\
	\hat{G}(\bm{k},t)
	&= 4 \pi c \frac{\sin ckt}{k} \Theta(t)
\end{align*}
%
mit der Heaviside-Funktion $\Theta(t)$ mit
%
\begin{align*}
	\Theta(t) &=
	\begin{cases}
		1 & , t>0 \\
		0 & , t<0 \\
	\end{cases}
\end{align*}

Im Ortsraum
%
\begin{align*}
	G(\bm{r},t)
	&= \int \frac{\mathrm{d}^3 k}{(2\pi)^3} \, \hat{G}(|\bm{k}|,t) \, \mathrm{e}^{\mathrm{i} \bm{k} \cdot \bm{r}}
	= G(|\bm{r}|,t)
\end{align*}
%
wähle nun speziell $\bm{r} = (0,0,r)$
%
\begin{align*}
	G(r,t)
	&= \frac{1}{(2\pi)^3} 4 \pi c \Theta(t) \int_{0}^{\infty} \mathrm{d}k \, k^2 \int_{0}^{\pi} \mathrm{d}\theta \, \sin\theta \int_{0}^{2\pi} \mathrm{d}\varphi \, \frac{\sin ckt}{k} \mathrm{e}^{\mathrm{i} k r \cos \theta} \\
	&= \frac{1}{\pi} c \theta(t) \int_{0}^{\infty} \mathrm{d}k \, k \sin ckt \underbrace{\int_{-1}^{1} \mathrm{d}x \, \mathrm{e}^{\mathrm{i}krx}}_{2\frac{\sin kr}{kr}} \\
	&= \frac{2 c \Theta(t)}{\pi r} \int_{0}^{\infty} \mathrm{d}k \, \sin(kr) \sin(ckt) \\
	&= \frac{c \Theta(t)}{\pi r} \int_{-\infty}^{\infty} \mathrm{d}k \, \sin(kr) \sin(ckt) \\
	G(r,t)
	&=
	\begin{multlined}[t]
		\frac{c \Theta(t)}{2 \pi r} \Biggl\{ \int_{-\infty}^{\infty} \mathrm{d}k \, [\cos(k(r-ct)) - \cos(k(r+ct))] \\
		+ \mathrm{i} \int_{-\infty}^{\infty} \mathrm{d}k \, [\sin(k(r-ct)) - \sin(k(r+ct))] \Biggr\} 
	\end{multlined}
	\\
	&= \frac{c \Theta(t)}{r} \int_{-\infty}^{\infty} \frac{\mathrm{d}k}{2\pi} \left[ \mathrm{e}^{\mathrm{i}k(r-ct)} - \mathrm{e}^{-\mathrm{i}k(r+ct)} \right] \\
	&= \frac{c \Theta(t)}{r} \int_{-\infty}^{\infty} \frac{\mathrm{d}k}{2\pi} \left\{ \delta(r-ct) - \delta(r+ct) \right\} \\
\intertext{da $\Theta(t)\delta(r+ct)=0$}
	&= \frac{c \Theta(t)}{r} \delta(r-ct) \\
	&= \frac{c \Theta(t)}{r} \delta\left( (-c) \left( t- \frac{r}{c} \right) \right) \\
	&= \frac{\Theta(t)}{r} \delta\left( t-\frac{r}{c} \right) \\
\intertext{da $\delta(t-r/c) = 0$ für $t<0$}
	&= \frac{\delta\left( t-\frac{r}{c} \right)}{r}
\end{align*}

Schlussendlich bleibt also übrig
%
\begin{align*}
	G(\bm{r}-\bm{r}',t-t')
	&= \frac{\delta\left( t-t'-\frac{|\bm{r}-\bm{r}'|}{c} \right)}{|\bm{r}-\bm{r}'|}
	\\
	\Aboxed{
		G_{\mathrm{ret.}}(\bm{r}-\bm{r}',t-t')
		&= \frac{\delta\left( t'+\frac{|\bm{r}-\bm{r}'|}{c}-t \right)}{|\bm{r}-\bm{r}'|}
	}
\end{align*}
%
Dies wird als \acct*{retardierte Greensche Funktion}\index{Greensche Funktion!retardierte} bezeichnet. $G_{\mathrm{ret.}}(\bm{r}-\bm{r}',t-t')$ ist eine vom Punkt $\bm{r}'$ auslaufenden Kugelwelle, die zur Zeit $t'$ losläuft.
%
\begin{enumerate}
	\item $t<t'$: Wert der Kugelwelle überall $0$

	\item $t=t'$: unendlich großer Wert (>>Blitz<<) bei $\bm{r}=\bm{r}'$

	\item $t>t'$: $\delta$-artige Intensität bei $|\bm{r}-\bm{r}'| = c(t-t')$
\end{enumerate}

Die Fläche konstanter Intensität ist eine Kugelschale um $\bm{r}'$ mit Radius $c(t-t') \to +\infty$, also einer Kugelwelle, die mit Lichtgeschwindigkeit ausläuft.

Es gibt noch eine weitere Greensche Funktion als Lösung.
%
\begin{figure}
	\centering
	\begin{pspicture}(-2.1,-2.1)(3,2.3)
		\psaxes[ticks=none,labels=none,arrows=->](0,0)(-2,-2)(2,2)[{\color{DimGray} $\Re \omega$}, 0][{\color{DimGray} $\Im \omega$}, 0]
		\psline[linecolor=DarkOrange3](-1.8,0)(1.8,0)
		\psline[linecolor=DarkOrange3,arrows=->](-1.1,0)(-1,0)
		\psline[linecolor=DarkOrange3,arrows=->](0.9,0)(1,0)
		\psarc[linestyle=none,arrows=->](0,0){1.8}{0}{45}
		\psarc[linestyle=none,arrows=->](0,0){1.8}{0}{135}
		\psarcn[linestyle=none,arrows=->](0,0){1.8}{0}{-135}
		\psarcn[linestyle=none,arrows=->](0,0){1.8}{0}{-45}
		\psarc(0,0){1.8}{0}{360}
		\cnode*[linecolor=MidnightBlue](-1,1){2pt}{A}
		\cnode*[linecolor=MidnightBlue](1,1){2pt}{B}
		\uput[-90](A){\color{MidnightBlue} $-ck-\mathrm{i}\varepsilon$}
		\uput[-90](B){\color{MidnightBlue} $ck-\mathrm{i}\varepsilon$}
		\pcarc(1.9;-60)(2,-1.5)\uput[0](2,-1.5){\color{DimGray} $t>0$}
		\pcarc(1.9;60)(2,1.5)\uput[0](2,1.5){\color{DimGray} $t<0$}
		\psline(1.6,0)(2.3,0.5)(1.8;20)\uput[0](2.3,0.5){\color{DimGray} $\mathcal{C}_1$}
		\psline(1.6,0)(2.3,-0.5)(1.8;-20)\uput[0](2.3,-0.5){\color{DimGray} $\mathcal{C}_2$}
		\rput(-1.5,2){\color{DimGray} $\boxed{\varepsilon < 0}$}
	\end{pspicture}
	\caption{Illustration zur anderen Verschiebung der Pole um eine neue Greensche Funktion zu erhalten.}
\end{figure}
%
\begin{align*}
	\tilde{G} = \lim\limits_{\varepsilon \to 0^-} \frac{2 \pi c}{k} \left[ \frac{1}{\omega -(-ck-\mathrm{i}\varepsilon)} - \frac{1}{\omega - (ck-\mathrm{i}\varepsilon)} \right]
\end{align*}
%
$\mathcal{C}_2$ umschließt keine Pole, folglich $\hat{G}(\bm{k},t>0) = 0$.
%
\begin{align*}
	\hat{G}(\bm{k},t<0)
	&= 2 \pi \mathrm{i} \left\{
		\Res \left[ \frac{1}{2 \pi} \tilde{G}(\bm{k},\omega) \, \mathrm{e}^{-\mathrm{i} \omega t} \right]_{\omega=-ck-\mathrm{i}\varepsilon}
		+ \Res \left[ \frac{1}{2 \pi} \tilde{G}(\bm{k},\omega) \, \mathrm{e}^{-\mathrm{i} \omega t} \right]_{\omega=ck-\mathrm{i}\varepsilon}
	\right\}	
\end{align*}

Duch analoge Rechnung zu oben erhalten wir
%
\begin{equation*}
	\begin{aligned}
		G(r,t)
		&= - \frac{c \Theta(t)}{r} \left\{ \delta(r-ct) - \delta(r+ct) \right\} \\
		&= \frac{c \Theta(-t)}{r} \delta(r+ct) \\
		&= \frac{\delta\left( t+\frac{r}{c} \right)}{r}
	\end{aligned}
\end{equation*}

Wir erhalten die \acct*{avancierte Greensche Funktion}\index{Greensche Funktion!avancierte}.
%
\begin{align*}
	\boxed{G_{\mathrm{av.}}(\bm{r}-\bm{r}',t-t') = \frac{\delta\left( t'-\frac{|\bm{r}-\bm{r}'|}{c}-t \right)}{|\bm{r}-\bm{r}'|}}
\end{align*}
%
\begin{figure}[htpb]
	\centering
	\begin{pspicture}(-4,-4)(3.5,4)
		\uput[180](-2.5,2.5){\color{DimGray} $t<t'$}
		\uput[180](-2.5,0){\color{DimGray} $t=t'$}
		\uput[180](-2.5,-2.5){\color{DimGray} $t>t'$}
		\uput[90](0,3.2){\color{DimGray} $\mathbb{R}^3$}

		\uput[90](-1.5,3.2){\color{DimGray} $G_{\mathrm{ret.}}$}
		\rput(-1.5,2.5){
			\psaxes[ticks=none,labels=none](0,0)(-0.5,-0.5)(0.5,0.5)
			\uput[-90](0,-0.5){\color{DimGray} nichts}
		}
		\rput(-1.5,0){
			\psaxes[ticks=none,labels=none](0,0)(-0.5,-0.5)(0.5,0.5)
			\psline[linecolor=DarkOrange3,arrows=->](0.5,0.5)(0.25,0.35)(0.35,0.35)(0.1,0.1)
			\uput[-90](0,-0.5){\color{DimGray} Blitz}
		}
		\rput(-1.5,-2.5){
			\psaxes[ticks=none,labels=none](0,0)(-0.5,-0.5)(0.5,0.5)
			\pscircle[linecolor=MidnightBlue](0,0){0.4}
			\psline[linecolor=DarkOrange3,arrows=->](0.2;45)(0.6;45)
			\psline[linecolor=DarkOrange3,arrows=->](0.2;135)(0.6;135)
			\psline[linecolor=DarkOrange3,arrows=->](0.2;225)(0.6;225)
			\psline[linecolor=DarkOrange3,arrows=->](0.2;315)(0.6;315)
			\uput[-90](0,-0.5){\color{DimGray} auslaufende Kugelwelle}
		}

		\uput[90](1.5,3.2){\color{DimGray} $G_{\mathrm{av.}}$}
		\rput(1.5,2.5){
			\psaxes[ticks=none,labels=none](0,0)(-0.5,-0.5)(0.5,0.5)
			\pscircle[linecolor=MidnightBlue](0,0){0.4}
			\psline[linecolor=DarkOrange3,arrows=<-](0.2;45)(0.6;45)
			\psline[linecolor=DarkOrange3,arrows=<-](0.2;135)(0.6;135)
			\psline[linecolor=DarkOrange3,arrows=<-](0.2;225)(0.6;225)
			\psline[linecolor=DarkOrange3,arrows=<-](0.2;315)(0.6;315)
			\uput[-90](0,-0.5){\color{DimGray} einlaufende Kugelwelle}
		}
		\rput(1.5,0){
			\psaxes[ticks=none,labels=none](0,0)(-0.5,-0.5)(0.5,0.5)
			\psline[linecolor=DarkOrange3,arrows=->](0.5,0.5)(0.25,0.35)(0.35,0.35)(0.1,0.1)
			\uput[-90](0,-0.5){\color{DimGray} Blitz}
		}
		\rput(1.5,-2.5){
			\psaxes[ticks=none,labels=none](0,0)(-0.5,-0.5)(0.5,0.5)
			\uput[-90](0,-0.5){\color{DimGray} nichts}
		}
	\end{pspicture}
	\caption{Vergleich von avancierter und retardierter Greenscher Funktion.}
\end{figure}
