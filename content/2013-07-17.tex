% Henri Menke, Michael Schmid, Marcel Klett, Jan Schnabel 2013 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

% Vorlesung am 17.07.2013
% TeX: Henri 

\renewcommand{\printfile}{2013-07-17}

Damit die Metrik einen Isomorphismus vermittelt, muss gelten:
%
\begin{align*}
	A^\nu = (g_{\mu\nu})^{-1} A_\mu
\end{align*}
%
wobei $(g_{\mu\nu})^{-1}$ die Umkehrabbildung ist. Definiere dazu
%
\begin{align*}
	\boxed{g^{\mu\kappa} g_{\kappa\nu} = \delta^\mu{}_\nu}
\end{align*}
%
Damit gilt
%
\begin{align*}
	A_\mu &= g_{\mu\nu} A^\nu \\
	\leadsto g^{\kappa\mu} A_\mu &= g^{\kappa\mu} g_{\mu\nu} A^\nu = \delta^\kappa{}_\nu A^\nu = A^\kappa
\end{align*}
%
\begin{align*}
	\boxed{A^\mu = g^{\mu\nu} A_\nu}
\end{align*}

\begin{theorem}[Behauptung]
	$g^{\mu\nu}$ ist ein $(2,0)$ Tensor, d.h.\ es muss gelten
	%
	\begin{align*}
		{g'}^{\mu\alpha} &= \Lambda^\mu{}_\kappa \Lambda^\alpha{}_\lambda g^{\kappa\lambda}
		{g'}^{\mu\alpha} {g'}_{\alpha\nu} = \delta^\mu{}_\nu
	\end{align*}
\end{theorem}

\begin{proof}
	\begin{align*}
		\Lambda^\mu{}_\alpha \Lambda^\alpha{}_\lambda \bar{\Lambda}^\beta{}_\alpha \bar{\Lambda}^\gamma{}_\nu g^{\kappa\lambda} g_{\beta\gamma}
		= \Lambda^\mu{}_\kappa \bar{\Lambda}^\gamma{}_\nu g^{\kappa\lambda} g_{\lambda\gamma}
		= \Lambda^\mu{}_\gamma \bar{\Lambda}^\gamma{}_\nu = \delta^\mu{}_\nu
	\end{align*}
\end{proof}

\begin{notice}
	Bisher wurde von keiner speziellen Raumdimension Gebrauch gemacht.

	Spezialfall: ($\mathbb{R}^3$, euklidische Metrik) $\mathbb{E}^3$.
	\[
		g_{ij} =
		\begin{pmatrix}
			1 & 0 & 0 \\
			0 & 1 & 0 \\
			0 & 0 & 1 \\
		\end{pmatrix}
	\]
\end{notice}

Die 4-diemnsionale >>Raum-Zeit<< der speziellen Relativitätstheorie ist durch die lorentzinvariante, indefinite Minkowski-Metrik (4-Metrik) charakterisiert.
%
\begin{align*}
	\boxed{
		(g_\mathrm{RZ})_{\mu\nu} = \eta_{\mu\nu} =
		\begin{pmatrix}
			-1 & 0 & 0 & 0 \\
			0 & 1 & 0 & 0 \\
			0 & 0 & 1 & 0 \\
			0 & 0 & 0 & 1 \\
		\end{pmatrix}
	}
\end{align*}

Postulat des invarianten Lichtkegels: Raum-Zeit Intervall $\mathrm{d}s^2 = \eta_{\mu\nu} \mathrm{d}x^\mu \, \mathrm{d}x^\nu$ ist lorentzinvariant, d.h.
%
\begin{align*}
	\Lambda^\alpha{}_\beta, \bar{\Lambda}^\alpha{}_\beta \; \text{sind lorentzinvariant} \iff \eta_{\alpha\beta} = {\eta'}_{\alpha\beta}
\end{align*}
%
\begin{gather*}
	\eta_{\alpha\beta} = {\eta'}_{\alpha\beta} \iff \eta_{\alpha\beta} = \bar{\Lambda}^\mu{}_\alpha \bar{\Lambda}^\nu{}_\beta \eta_{\mu\nu} \\
	\Lambda^\alpha{}_\kappa \Lambda^\beta{}_\lambda \eta_{\alpha\beta} = \Lambda^\alpha{}_\kappa \Lambda^\beta{}_\lambda \bar{\Lambda}^\mu{}_\alpha \bar{\Lambda}^\nu{}_\beta \eta_{\mu\nu} = \eta_{\kappa\lambda} \\
	\eta_{\alpha\beta} = \Lambda^\mu{}_\alpha \Lambda^\nu{}_\beta \eta_{\mu\nu}
\end{gather*}

In der Standardkonfiguration
%
\begin{gather*}
	{x'}^0 = \gamma \left( x^0 - \frac{v}{c} x^1 \right),
	{x'}^1 = \gamma \left( x^1 - \frac{v}{c} x^0 \right),
	{x'}^2 = x^2,
	{x'}^3 = x^3,
	\gamma = \left( 1 - \frac{v^2}{c^2} \right)^{-1/2}
\end{gather*}
%
und mit
%
\begin{align*}
	\Lambda^\mu{}_\nu = \frac{\partial {x'}^\mu}{\partial x^\nu}, 
	\Lambda^\mu{}_\nu =
	\begin{pmatrix}
		\gamma & -\gamma \frac{v}{c} & 0 & 0 \\
		-\gamma \frac{v}{c} & \gamma & 0 & 0 \\
		0 & 0 & 1 & 0 \\
		0 & 0 & 0 & 1 \\
	\end{pmatrix}
	= \Lambda^\nu{}_\mu
\end{align*}
%
erhält man
%
\begin{align*}
	\eta_{\alpha\beta}
	&= \Lambda^\mu{}_\alpha \Lambda^\nu{}_\beta \eta_{\mu\nu}
	= \Lambda^\mu{}_\alpha \eta_{\mu\nu} \Lambda^\nu{}_\beta
	= \Lambda^\alpha{}_\mu \eta_{\mu\nu} \Lambda^\nu{}_\beta \\
	&=
	\begin{pmatrix}
		\gamma & -\gamma \frac{v}{c} & 0 & 0 \\
		-\gamma \frac{v}{c} & \gamma & 0 & 0 \\
		0 & 0 & 1 & 0 \\
		0 & 0 & 0 & 1 \\
	\end{pmatrix}
	\begin{pmatrix}
		-1 & 0 & 0 & 0 \\
		0 & 1 & 0 & 0 \\
		0 & 0 & 1 & 0 \\
		0 & 0 & 0 & 1 \\
	\end{pmatrix}
	\begin{pmatrix}
		\gamma & -\gamma \frac{v}{c} & 0 & 0 \\
		-\gamma \frac{v}{c} & \gamma & 0 & 0 \\
		0 & 0 & 1 & 0 \\
		0 & 0 & 0 & 1 \\
	\end{pmatrix} \\
	&=
	\begin{pmatrix}
		\gamma & -\gamma \frac{v}{c} & 0 & 0 \\
		-\gamma \frac{v}{c} & \gamma & 0 & 0 \\
		0 & 0 & 1 & 0 \\
		0 & 0 & 0 & 1 \\
	\end{pmatrix}
	\begin{pmatrix}
		-\gamma & -\gamma \frac{v}{c} & 0 & 0 \\
		-\gamma \frac{v}{c} & \gamma & 0 & 0 \\
		0 & 0 & 1 & 0 \\
		0 & 0 & 0 & 1 \\
	\end{pmatrix} \\
	&=
	\begin{pmatrix}
		-\gamma^2 + \gamma^2 \frac{v^2}{c^2} & \gamma^2 \frac{v^2}{c^2} - \gamma^2 \frac{v^2}{c^2} & 0 & 0 \\
		\gamma^2 \frac{v}{c} - \gamma^2 \frac{v}{c} & -\gamma^2 \frac{v^2}{c^2} + \gamma^2 & 0 & 0 \\
		0 & 0 & 1 & 0 \\
		0 & 0 & 0 & 1 \\
	\end{pmatrix}
	=
	\begin{pmatrix}
		-1 & 0 & 0 & 0 \\
		0 & 1 & 0 & 0 \\
		0 & 0 & 1 & 0 \\
		0 & 0 & 0 & 1 \\
	\end{pmatrix} \\
\end{align*}

\begin{notice}[Folgerungen:]
	\begin{itemize}
		\item $A^\mu = (A^0, \bm{A}) \iff A_\mu = g_{\mu\nu} A^\nu = (-A^0, \bm{A})$

		\item $A^\mu A_\mu = \eta_{\mu\nu} A^\mu A^\nu = - (A^0)^2 + \bm{A}^2$

		\item $A^i = A_i , i = 1,2,3$

		\item $\eta^{\alpha\beta} \eta_{\beta\gamma} = \delta^\alpha{}_\gamma$
			\[ \implies \eta^{\alpha\beta} = \eta_{\alpha\beta}
				\begin{pmatrix}
					-1 & 0 & 0 & 0 \\
					0 & 1 & 0 & 0 \\
					0 & 0 & 1 & 0 \\
					0 & 0 & 0 & 1 \\
				\end{pmatrix}
			\]
	\end{itemize}
\end{notice}

\section{Relativistische Mechanik}

\begin{figure}[htb]
	\centering
	\begin{pspicture}(-0.3,-0.3)(2,2)
		\psaxes[labels=none,ticks=none,arrows=->](0,0)(-0.3,-0.3)(3,2)[{\color{DimGray} $\bm{r}$},-90][{\color{DimGray} $x^0 = ct$},180]
		\pscurve[linecolor=MidnightBlue,arrows=->](2.5,-0.3)(2,1)(2.3,2)
		\psline[arrows=->](0,0)(2,1)
		\psline[linecolor=DarkOrange3,arrows=->](2,1)(2,2)
		\uput[120](1,0.5){\color{DimGray} $x^\mu(\tau)$}
		\uput[180](2,2){\color{DarkOrange3} $u^\mu(\tau)$}
	\end{pspicture}
	\caption{Die Tangente an der Weltlinie ist die Geschwindigkeit.}
\end{figure}

Kurvenparameter der Weltlinie $x^\mu$: Eigenzeit $\tau$, die wegen \[ \mathrm{d}\tau^2 = - \frac{\mathrm{d}s^2}{c^2} \] eine Lorentzinvariante und damit ein Skalar ist.

Der Tangentialvektor an der Weltlinie ist \[ \frac{\mathrm{d}x^\mu}{\mathrm{d}\tau} \] Dies gibt den Anlass zur Einführung der 4-Geschwindigkeit
%
\begin{align*}
	\boxed{u^\mu = \frac{\mathrm{d}x^\mu(\tau)}{\mathrm{d}\tau} = \frac{1}{\sqrt{1 - \beta^2(\tau)}} \left. \frac{\mathrm{d}x^\mu(t)}{\mathrm{d}t} \right|_{t = t(\tau)}}
\end{align*}
%

\begin{notice}
	\begin{itemize}
		\item $\beta^2 = \left( \frac{\bm{v}(\tau)}{c} \right)^2$

		\item $u^\mu$ ist ein 4-Vektor, da $\mathrm{d}x^\mu$ ein 4-Vektor und $\mathrm{d}\tau$ ein Skalar.

		\item $x^\mu(t) = (xt,\bm{r}(t)) , \bm{v}(t) = \frac{\mathrm{d}\bm{r}(t)}{\mathrm{d}t}$
			\[ \implies
				u^\mu = \frac{1}{\sqrt{1 - \beta^2(\tau)}} \left( ct, \frac{\mathrm{d}\bm{r}(t)}{\mathrm{d}t} \right) = \left( \frac{c}{\sqrt{1 - \frac{v^2}{c^2}}}, \frac{\bm{v}}{\sqrt{1 - \frac{v^2}{c^2}}} \right) 
			\]
			wobei $\bm{v} = \bm{v}(\tau) = \left. \frac{\mathrm{d}\bm{r}(t)}{\mathrm{d}t} \right|_{t = t(\tau)}$

		\item 3-Teil des 4-Vektors $u^\mu = \frac{\bm{v}}{\sqrt{1 - v^2/c^2}}$, d.h.\ $\bm{v} = \frac{\mathrm{d}\bm{r}(t)}{\mathrm{d}t}$ ist nicht 3-Teil eines 4-Vektors

		\item $u^\mu u_\mu = - \frac{c^2}{1 - v^2/c^2} + \frac{v^2}{1 - v^2/c^2} = - \frac{c^2}{1 - v^2/c^2} \left( 1 - \frac{v^2}{c^2} \right) = -c^2$, zeitartig.
	\end{itemize}
\end{notice}

\paragraph{2 Wege zur relativistischen Mechanik}

\begin{enumerate}
	\item lorentzkovariante Verallgemeinerung der Newtonschen Mechanik, die sich im Limes $v/c \ll 1$ aus der relativistischen Mechanik ergibt.

	\item Invariantes Wirkungsprinzip: $L(v^2) \, \mathrm{d}t = L({v'}^2) \, \mathrm{d}t'$ für Lorentztransformation.
\end{enumerate}

\textbf{Weg 2:} Lagrangefunktion eines freien, relativistischen Teilchens:
\[ L(v^2) = - m c^2 \sqrt{1 - \frac{v^2}{c^2}} \; , \quad \text{$m$: invariante Ruhemasse} \]
Die Euler-Lagrange-Gleichungen führen auf
\[ \frac{\mathrm{d}}{\mathrm{d}t} \left[ - m c^2 \frac{\bm{v}}{\sqrt{1 - v^2/c^2}} \right] = 0 \]
\[ L(v) = - m c^2 \sqrt{1 - \frac{v^2}{c^2}} = - m c^2 \sqrt{1 - \frac{\dot{q}^2}{c^2}} \]

\begin{align*}
	\frac{\partial L}{\partial \dot{q}}
	&= - m c^2 \frac{- \frac{2 \dot{q}}{c^2}}{2 \sqrt{1 - \frac{\dot{q}^2}{c^2}}}
	= \frac{m \dot{q}}{\sqrt{1 - \frac{\dot{q}^2}{c^2}}}
	= p \\
	p^2 &= \frac{m^2 \dot{q}^2}{1 - \frac{\dot{q}^2}{c^2}} \\
%
	m^2 \dot{q}^2 &= p^2 - \frac{p^2 \dot{q}^2}{c^2}
	\implies \dot{q}^2 \left\{ m^2 + \frac{p^2}{c^2} \right\} = p^2
	\implies \dot{q}^2 = \frac{p^2}{m^2 + p^2/c^2} \\
	\dot{q} &= \frac{p}{\sqrt{m^2 + p^2/c^2}} \\
%
	H(p) &= p \dot{q}(p) L(\dot{q}(p)) = p \frac{p}{\sqrt{m^2 + p^2/c^2}} + m c^2 \sqrt{1 - \frac{\dot{q}^2}{c^2}} \\
%
	\sqrt{1 - \frac{\dot{q}^2}{c^2}} &= \sqrt{1 - \frac{p^2/c^2}{m^2 + p^2/c^2}} = \sqrt{\frac{m^2 + p^2/c^2 - p^2/c^2}{m^2 + p^2/c^2}} = \frac{m}{\sqrt{m^2 + p^2(c^2}}
\end{align*}
%
\begin{align*}
	\implies H(p)
	&= \frac{p^2}{\sqrt{m^2 + p^2/c^2}} + \frac{m^2 c^2}{\sqrt{m^2 + p^2/c^2}} \\
	&= \frac{p^2 + m^2 c^2}{\sqrt{m^2 + p^2/c^2}} \\
	&= \frac{c^2 (m^2 + p^2/c^2)}{\sqrt{m^2 + p^2/c^2}} \\
	&= c^2 \sqrt{m^2 + p^2/c^2} \\
%
	\Aboxed{H(p) &= c \sqrt{m^2 c^2 + p^2}}
\end{align*}

Nichtrelativistischer Grenzfall:
%
\begin{align*}
	H(p \to 0) &= m^2 c^2 \sqrt{1 + \frac{p^2}{m^2 c^2}} \\
	\leadsto{}& m c^2 \left\{ 1 + \frac{p^2}{2 m^2 c^2} + \dots \right\} \\
	\leadsto{}& m c^2 + \frac{p^2}{2 m} + \dots
\end{align*}

\textbf{Weg 1:} Trajektorie für ein kräftefreies Teilchen. Nach Newton gilt
%
\begin{align*}
	\frac{\mathrm{d}}{\mathrm{d}t} (m \bm{v}) = 0
\end{align*}
%
d.h.\ $m \bm{v} = \bm{p} = \mathrm{const}$.

Nach Einstein:
%
\begin{align*}
	p^\mu = m u^\mu = \left( \frac{m c}{\sqrt{1 - \beta^2}}, \frac{m \bm{v}}{\sqrt{1 - \beta^2}} \right)
\end{align*}
%
ist ein 4-Vektor mit $\partial_\tau p^\mu = 0$.

Bewegungsgesetz nach Newton
%
\begin{align*}
	\frac{\mathrm{d}}{\mathrm{d}t} (m \bm{v}) = \bm{F} \leadsto \frac{\mathrm{d}}{\mathrm{d}t} (\frac{m \bm{v}^2}{2}) = \bm{F} \cdot \bm{v}
\end{align*}
%
und nach Einstein
%
\begin{align*}
	\frac{\mathrm{d}}{\mathrm{d}\tau} p^\mu = m \frac{\mathrm{d} u^\mu}{\mathrm{d}\tau} = K^\mu = (K^0, \bm{K})
\end{align*}
%
manifest lorentzkovariant wenn $K^\mu$ ein 4-Vektor ist.

3-Teil:
%
\begin{align*}
	\frac{\mathrm{d}}{\mathrm{d}\tau} \left( \frac{m \bm{v}}{\sqrt{1 - \beta^2}} \right) &= \bm{K} \\
	\frac{1}{\sqrt{1 - \beta^2}} \frac{\mathrm{d}}{\mathrm{d}t} \left( \frac{m \bm{v}}{\sqrt{1 - \beta^2}} \right) &= \bm{K} \\
	\frac{\mathrm{d}}{\mathrm{d}t} \left( \frac{m \bm{v}}{\sqrt{1 - \beta^2}} \right) &= \frac{1}{\sqrt{1 - \beta^2}} \bm{K} = \bm{F}
\end{align*}
%
wobei $m/\sqrt{1 - \beta^2}$ die dynamische Masse ist.

\begin{notice}
	$\bm{F}$ ist im Gegensatz zu $\bm{K}$ nicht der 3-Teil von einer 4-Kraft (Analog wie $\bm{v}$ nicht 3-Teil von $u^\mu$ ist).
\end{notice}

0-Teil:
%
\begin{align*}
	u^\mu u_\mu &= - c^2 \\
	\leadsto 0 &= \frac{\mathrm{d}}{\mathrm{d}\tau} (u^\mu u_\mu) = \frac{\mathrm{d} u^\mu}{\mathrm{d}\tau} u_\mu + u^\mu \frac{\mathrm{d} u_\mu}{\mathrm{d}\tau} = 2 \frac{\mathrm{d} u^\mu}{\mathrm{d}\tau} u_\mu \\
	\implies \frac{\mathrm{d} u^\mu}{\mathrm{d}\tau} u_\mu &= 0 \\
	\leadsto \frac{\mathrm{d} u^\mu}{\mathrm{d}\tau} &= K^\mu \leadsto m \frac{\mathrm{d} u^\mu}{\mathrm{d}\tau} u_\mu = K^\mu u_\mu = 0 \\
	\implies K^0 u_0 + K^i u_i &= 0 \implies - K^0 u^0 + \bm{K} \bm{u} = 0 \\
	\leadsto K^0 &= \frac{\bm{k} \bm{u}}{u^0} = \frac{1}{c/\sqrt{1-\beta^2}} \left[ \frac{\bm{F}}{\sqrt{1-\beta^2}} \cdot \frac{\bm{v}}{\sqrt{1-\beta^2}} \right] = \frac{1}{c} \frac{\bm{F} \cdot \bm{v}}{\sqrt{1-\beta^2}} \\
	K^0 &= \frac{\mathrm{d} p^0}{\mathrm{d}\tau} = m \frac{\mathrm{d} p^0}{\mathrm{d}\tau} = \frac{m}{\sqrt{1-\beta^2}} \frac{\mathrm{d}}{\mathrm{d}t}\left( \frac{c}{\sqrt{1-\beta^2}} \right) = \frac{1}{c} \frac{1}{\sqrt{1-\beta^2}} \bm{F} \cdot \bm{v} \\
	\implies{}& \frac{\mathrm{d}}{\mathrm{d}t} \left( \frac{m c^2}{\sqrt{1-\beta^2}} \right) = \bm{F} \cdot \bm{v}
\end{align*}
%
klassisch:
%
\begin{align*}
	\frac{\mathrm{d}}{\mathrm{d}t} \left( \frac{1}{2} m v^2 \right) &= \bm{F} \cdot \bm{v}
\end{align*}
%
Damit folgt
%
\begin{align*}
	\boxed{E = \frac{m c^2}{\sqrt{1-\beta^2}} + \mathrm{const}}
\end{align*}

\begin{align*}
	p^\mu &= \left( \frac{m c}{\sqrt{1-\beta^2}}, \frac{m \bm{v}}{\sqrt{1-\beta^2}} \right) \\
	\Aboxed{p^\mu &= \left( \frac{E}{c}, \bm{p} \right) \; \text{mit} \; \bm{p} = \gamma m \bm{v} \; \text{und} \; E = \gamma m c^2 , \gamma = \frac{1}{\sqrt{1-\beta^2}}}
\end{align*}


