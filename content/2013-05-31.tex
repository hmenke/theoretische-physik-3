% Henri Menke, Michael Schmid, Marcel Klett, Jan Schnabel 2013 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

% Vorlesung am 31.05.2013
% TeX: Marcel

\renewcommand{\printfile}{2013-05-31}

\begin{example} Die geerdete, leitende Kugel

	\begin{figure}[htpb]
		\centering
		\begin{pspicture}(-2.5,-1.8)(4,1.5)
			\cnode*(0,0){2pt}{O}
			\psline[arrows=->](O)(4;0)
			\psline[arrows=->](O)(2;45)
			\psarc[arrows=->](O){1.5}{0}{45}
			\pscircle(O){1}
			\psline(O)(1;120)
			\psline[arrows=->](1;140)(1.5;140)
			\cnode*[linecolor=MidnightBlue](3,0){2pt}{q1}
			\cnode*[linecolor=DarkOrange3](0.7,0){2pt}{q2}
			\ncline[arrows=->,linecolor=MidnightBlue]{q2}{q1}
			\ncline[arrows=->,linecolor=DarkOrange3]{O}{q2}
			\psline(0,-1)(0,-1.5)
			\psline(-0.5,-1.5)(0.5,-1.5)
			\psline(-0.25,-1.6)(0.25,-1.6)
			\psline(-0.125,-1.7)(0.125,-1.7)
			\psline[arrows=->](-0.1,-1.1)(-0.1,-1.4)
			\psline[arrows=<-](0.1,-1.1)(0.1,-1.4)
			% Labels
			\uput[135](1.5;45){\color{DimGray} $\bm{r}$}
			\uput{1.2}[22.5](0,0){\color{DimGray} $\vartheta$}
			\uput[-90](q1){\color{MidnightBlue} $q$}
			\uput[-90](2;0){\color{MidnightBlue} $\bm{a}$}
			\uput[-90](0.7;0){\color{DarkOrange3} $-q$}
			\uput[-90](0.25;0){\color{DarkOrange3} $\bm{a}'$}
			\uput[50](1.5;140){\color{DimGray} $\bm{n}$}
			\uput[210](0.5;120){\color{DimGray} $R$}
			\uput[-90](4;0){\color{DimGray} $x$}
			\uput[0](0.1,-1.25){\color{DimGray} Ladung}
			\pcarc[arcangle=-20,arrows=->](-1.3,-1.3)(1;-135)
			\uput[180](-1.3,-1.3){\color{DimGray} $\phi|_{\partial L} = 0$}
		\end{pspicture}
		\caption{Eine geerdete, leitende Kugel und eine außerhalb ruhende Ladung $q$.}
	\end{figure} 

	Im Folgenden wollen wir einen Ausdruck für das Potential $\phi(\bm{r})$ außerhalb der Kugel suchen. Zudem wissen wir, dass der Kugelmantel eine Äquipotentialfläche sein muss, mit der Bedingung $\phi|_{r=R} = 0$ (geerdet).
	%
	Als Ansatz benutzen wir ein Potential, welches die Poissongleichung für $r>R$ löst.
	%
	\begin{equation*}
		\phi(\bm{r}) = \frac{q}{\left|\bm{r} - \bm{a} \right|} - \frac{q'}{\left| \bm{r} - \bm{a}' \right|}
	\end{equation*}
	%
	Betrachten wir nun die Randbedingung so erhalten wir
	%
	\begin{align*}
		&\phi_{|r=R} = 0 = \frac{q}{\left|R \bm{\hat{r}} - a \bm{\hat{x}} \right|} - \frac{q'}{\left|R \bm{\hat{r}} - a' \bm{\hat{x}} \right|}  \\
		&\implies \left( \frac{q}{q'}\right)^2 = \frac{\left|R \bm{\hat{r}} - a \bm{\hat{x}} \right|^2}{\left|R \bm{\hat{r}} - a' \bm{\hat{x}} \right|^2} = \frac{R^2 + a^2 - 2 R a \cos\vartheta}{R^2 + a'^2 - 2 R a' \cos\vartheta} \\
		&\underbrace{\left( \frac{q}{q'}\right)^2 \left(R^2 + a'^2 \right) - (R^2 + a^2)}_{=0 \text{ \eqref{eq:2013-05-31-1}}} - \Biggl[\underbrace{\left( \frac{q}{q'}\right)^2 2 R a' - 2 R a}_{=0 \text{ \eqref{eq:2013-05-31-2}}} \Biggr] \cos\vartheta = 0 
	\end{align*}
	%
	Die beiden Terme müssen unabhängig voneinander verschwinden, da die Randbedingung unabhängig vom Winkel sein muss. Man erhält nun also zwei Bedingungen, bzw.\ zwei Gleichungen für zwei Unbekannte
	%
	\begin{align}
		\left( \frac{q}{q'}\right)^2 &= \frac{R^2 + a^2}{R^2 + a'^2} \label{eq:2013-05-31-1} \\
		\left( \frac{q}{q'}\right)^2 &= \frac{a}{a'} \label{eq:2013-05-31-2}
	\end{align}
	%
	Gleichsetzen der beiden Gleichungen liefert
	%
	\begin{align*}
		\frac{R^2 + a^2}{R^2 + a'^2} = \frac{a}{a'} \implies a'^2 - a' \left( a + \frac{R^2}{a} \right) + R^2 = 0
	\end{align*}
	%
	Diese quadratische Gleichung besitzt die zwei Lösungen
	%
	\begin{align*}
		a'_{1/2}
		&= \frac{1}{2} \left(a + \frac{R^2}{a}  \right) \pm \sqrt{\frac{1}{4} \left(a + \frac{R^2}{a}\right)^2 - R^2} \\
		&= \frac{1}{2} \left(a + \frac{R^2}{a}\right) \pm \frac{1}{2} \left(a - \frac{R^2}{a}\right) \\
		&=
		\begin{dcases}
			a & \\
			\frac{R^2}{a} & \text{richtige Lösung}
		\end{dcases}
	\end{align*}
	%
	Wir betrachten hierbei nur eine Lösung der quadratischen Gleichung, da die zweite $a' = a$ wenig Sinn ergibt. Wir erhalten schließlich für die Position unserer Spiegelladung
	%
	\begin{equation*}
		a' \cdot a = R^2
	\end{equation*}
	%
	Dieses Ergebnis ist auch als Spiegelung am Kreis bekannt. Setzen wir nun unser Ergebnis in eine der zwei Bedingungen von oben ein, so ergibt sich
	%
	\begin{align*}
		\left( \frac{q}{q'}\right)^2 = \frac{a'}{a} = \frac{R^2}{a^2} \implies q' = \pm q \frac{R}{a}
	\end{align*}
	%
	Wir betrachten nur die Lösung $q' = q \frac{R}{a}$, da nur sie die Bedingung $\phi|_{r=R} = 0$ erfüllen kann. Der Lohn unserer Arbeit besteht darin, das Potential nun in Abhängigkeit von $R$ und $a$ für den Außenraum niederschreiben zu können
	%
	\begin{equation*}
		\phi(\bm{r}) = q \left( \frac{1}{\left| \bm{r} - \bm{a}\right|} - \frac{R}{a} \frac{1}{\left|\bm{r} - \frac{R^2}{a} \bm{a}\right|}\right)
	\end{equation*}
	%
	Betrachten wir nun die Influenzoberflächenladungsdichte $\sigma$ der Kugel. Dazu schreiben wir zuerst einmal das Potential um, welches offensichtlich invaraint unter Rotation um den Winkel $\varphi$ ist.
	%
	\begin{align*}
		\phi(r,\vartheta) = q \left[ \left( r^2 + a^2 - 2  r  a  \cos\vartheta \right)^{-\frac{1}{2}
		}  - \frac{R}{a} \left(   r^2 +\frac{R^4}{a^2}- 2 R^2 \frac{r}{a} \cos\vartheta \right)^{-\frac{1}{2}} \right]
	\end{align*}
	%
	Zudem wissen wir, dass folgender Zusammenhang zwischen dem Potential und der Influenzoberflächenladungsdichte besteht
	%
	\begin{equation*}
		\sigma = - \frac{1}{4 \pi} \bm{e}_r \cdot \nabla \phi|_{r=R} = - \frac{1}{4 \pi} \left. \frac{\partial \phi}{\partial_r} \right|_{r=R}
	\end{equation*}
	%
	Setzen wir nun unser Potential ein, so erhalten wir
	%
	\begin{align*}
		\sigma &= -\frac{q}{4 \pi} \left( - \frac{1}{2} \frac{2r - 2a \cos\vartheta}{\left[ r^2 +a^2 - 2 r a\cos\vartheta  \right]^{\frac{3}{2}}} + \frac{1}{2} \frac{R}{a} \frac{2r - 2 \frac{R^2}{a} \cos\vartheta}{\left[ r^2 + \frac{R^4}{a^2} - 2R^2  \frac{r}{a} \cos\vartheta \right]^{\frac{3}{2}}}\right)_{r=R} \\
		%
		&= -\frac{q}{4 \pi} \left( \frac{-R + a \cos\vartheta}{\left[ R^2 +a^2 - 2 R a\cos\vartheta  \right]^{\frac{3}{2}}} +  \frac{\frac{R^2}{a} - \frac{R^3}{a^2} \cos\vartheta}{\left[ R^2 + \frac{R^4}{a^2} - 2  \frac{R^3}{a} \cos\vartheta \right]^{\frac{3}{2}}}\right) \\
		&=-\frac{q}{4 \pi} \left( \frac{R\left(-1 + \frac{a}{R} \cos\vartheta \right)}{R^3 \left[ 1 + \left(\frac{a}{R}\right)^2 - 2 \frac{a}{R} \cos\vartheta  \right]^{\frac{3}{2}}} +  \frac{R \left( \frac{R}{a} - \frac{R^2}{a^2} \cos\vartheta \right)  }{R^3 \left[ 1 + \left(\frac{R}{a}\right)^2 - 2  \frac{R}{a} \cos\vartheta \right]^{\frac{3}{2}}}\right) \\
		%
		&=-\frac{q}{4 \pi R^2} \left( \frac{-1 + \frac{a}{R} \cos\vartheta }{\left[ 1 + \left(\frac{a}{R}\right)^2 - 2 \frac{a}{R} \cos\vartheta  \right]^{\frac{3}{2}}} +  \frac{R}{a} \frac{ 1 - \frac{R}{a} \cos\vartheta   }{ \left[ 1 + \left(\frac{R}{a}\right)^2 - 2  \frac{R}{a} \cos\vartheta \right]^{\frac{3}{2}}}\right) \\
		%
		&=-\frac{q}{4 \pi R^2} \left( \frac{-1 + \frac{a}{R} \cos\vartheta }{\left(\frac{a}{R}\right)^3 \left[ 1 + \left(\frac{R}{a}\right)^2 - 2 \frac{R}{a} \cos\vartheta  \right]^{\frac{3}{2}}} +  \frac{R}{a} \frac{ 1 - \frac{R}{a} \cos\vartheta   }{ \left[ 1 + \left(\frac{R}{a}\right)^2 - 2  \frac{R}{a} \cos\vartheta \right]^{\frac{3}{2}}}\right) \\
		%
		& = -\frac{q}{4 \pi R^2} \frac{- \left(\frac{R}{a}\right)^3 + \left(\frac{R}{a}\right)^2 \cos\vartheta + \frac{R}{a} - \frac{R^2}{a^2} \cos\vartheta}{\left[ 1 + \left(\frac{R}{a}\right)^2 - 2  \frac{R}{a} \cos\vartheta \right]^{\frac{3}{2}}} \\
		%
		\Aboxed{
			\sigma(\vartheta) & = -\frac{q}{4 \pi R^2} \frac{\frac{R}{a} \left( 1- \left(\frac{R}{a}\right)^2 \right)}{\left[ 1 + \left(\frac{R}{a}\right)^2 - 2  \frac{R}{a} \cos\vartheta \right]^{\frac{3}{2}}}
		}
	\end{align*}

	\begin{figure}[htpb]
		\centering
		\def\PSTBox{
			\rput[lb](0,0){
				\begin{pspicture}(-0.3,-0.3)(4,3.5)
					\psline[linewidth=0pt](\psPi,-0.1)(\psPi,0.15)
					\psplot[linecolor=MidnightBlue,linewidth=0pt]{0}{\psPi}{0.5 * (1 - ( 0.5 )^2 ) / ( 1 + ( 0.5 )^2 - 2 * 0.5 * cos(x) )^(3/2)}
					\psplot[linecolor=DarkOrange3,linewidth=0pt]{0}{\psPi}{0.25 * (1 - ( 0.25 )^2 ) / ( 1 + ( 0.25 )^2 - 2 * 0.25 * cos(x) )^(3/2)}
				\end{pspicture}
			}
		}
		\begin{pspicture}(-0.8,-0.6)(4.5,3.8)
			\psaxes[labels=y,ticks=y,labelFontSize=\color{DimGray},arrows=->](0,0)(-0.3,-0.3)(4,3.5)[{\color{DimGray} $\vartheta$},-90][{\color{DimGray} $\frac{4 \pi R^2 \sigma}{-q}$}, 0]
			\psxTick(\psPi){\color{DimGray} \pi}
			\psyTick(0.5555){\color{DimGray} 5/9}
			\psplot[linecolor=MidnightBlue]{0}{\psPi}{0.5 * (1 - ( 0.5 )^2 ) / ( 1 + ( 0.5 )^2 - 2 * 0.5 * cos(x) )^(3/2)}
			\psplot[linecolor=DarkOrange3]{0}{\psPi}{0.25 * (1 - ( 0.25 )^2 ) / ( 1 + ( 0.25 )^2 - 2 * 0.25 * cos(x) )^(3/2)}
			\rput(1,2){\color{MidnightBlue} $\frac{R}{a} = \frac{1}{2}$}
			\rput(2,1){\color{DarkOrange3} $\frac{R}{a} = \frac{1}{4}$}
			
			\pcarc[arrows=->](3.5,1)(\psPi,0.1)
			\rput(0,1){\PstLens[LensHandle=false,LensSize=0.8,LensMagnification=80](3.532,0.417){\PSTBox}}
			\rput(3.7,1.7){\color{DarkOrange3} $\frac{3}{25}$}
			\rput(3.7,1){\color{MidnightBlue} $\frac{3}{27}$}
		\end{pspicture}
		\caption{Influenzoberflächenladungsdichte der geerdeten, leitenden Kugel.}
	\end{figure} 

	% ab hier TeX: Henri

	\paragraph{Check:}
	%
	\begin{align*}
		\int \underbrace{R^2 \mathrm{d}\Omega}_{\mathrm{d}f} \sigma
		&=
		\begin{aligned}[t]
			2 \pi \int_{0}^{2\pi} \mathrm{d}\vartheta \, \sin\vartheta \left[ - \frac{q}{4 \pi} \right] \frac{\frac{R}{a} \left( 1 - \left( \frac{R}{a} \right)^2 \right)}{\left[ 1 + \left(\frac{R}{a}\right)^2 - 2  \frac{R}{a} \cos\vartheta \right]^{3/2}} \\
			- \frac{q}{2} \frac{R}{a} \left( 1 - \left( \frac{R}{a} \right)^2 \right) \underbrace{\int_{-1}^{1} \mathrm{d}x \, \left[ 1 + \left( \frac{R}{a} \right)^2 - 2 \frac{R}{a} x \right]^{-3/2}}_{\frac{a}{R} \int_{-1}^{1} \mathrm{d}x \, \frac{\mathrm{d}}{\mathrm{d}x} \left[ 1 - \left( \frac{R}{a} \right)^2 - 2 \frac{R}{a} x \right]}
		\end{aligned}
		 \\
		 &= - \frac{q}{2} \left( 1 - \left( \frac{R}{a} \right)^2 \right) \left\{ \frac{1}{1 - \frac{R}{a}} - \frac{1}{1 + \frac{R}{a}} \right\} \\
		 &= - \frac{q}{2} 2 R a = - q \frac{R}{a} = - q'
	\end{align*}
	%
	Mit dem Satz von Gauß:
	%
	\begin{align*}
		\int_{\partial \mathrm{Kugel}} \bm{E} \cdot \mathrm{d}\bm{f}
		&= \int_{\mathrm{Kugel}} \div \bm{E} \, \mathrm{d}V \\
		&= \int_{\mathrm{Kugel}} 4 \pi \varrho(\bm{r}) \, \mathrm{d}V \\
		&= \int_{\mathrm{Kugel}} (-q') \delta(\bm{r} - \bm{a}') \, \mathrm{d}V \\
		&= - 4 \pi q' \\
		\int_{\partial \mathrm{Kugel}} \bm{E} \cdot \mathrm{d}\bm{f}
		&= \int_{\partial \mathrm{Kugel}} \bm{E} \cdot \bm{n} \, \mathrm{d}f \\
		&= 4 \pi \int_{\partial \mathrm{Kugel}} \sigma \, \mathrm{d}f \\
		\implies \int_{\partial \mathrm{Kugel}} \sigma \, \mathrm{d}f &= -q'
	\end{align*}

	Die Kraft, die auf die Ladung $q$ wirkt kann wie folgt berechnet werden:
	%
	\begin{align*}
		\bm{F} &= \int \frac{\sigma  \, \mathrm{d}f \cdot q}{|\bm{a} - \bm{R}|^3} (\bm{a} - \bm{R})
	\intertext{Wähle nun $\bm{a}$ entlang der $z$-Achse}
		&= R^2 q \int_{0}^{2 \pi} \mathrm{d}\varphi \int_{0}^{\pi} \mathrm{d}\vartheta \, \sin\vartheta \frac{\sigma}{[a^2 + R^2 - 2 a R \cos\vartheta]^{3/2}} 
		\begin{pmatrix}
			- R \sin\vartheta \cos\varphi \\
			- R \sin\vartheta \sin\varphi \\
			a - R \cos\vartheta \\
		\end{pmatrix} \\
		&=
		\begin{pmatrix}
			0 \\ 0 \\ F \\
		\end{pmatrix} \\
		F &=
		R^2 q \left( - \frac{q}{4 \pi R^2} \right) \frac{R}{a} \left( 1 - \left( \frac{R}{a} \right)^2 \right) \frac{2 \pi}{a^2} \int_{-1}^{1} \mathrm{d}x \frac{1 - \frac{R}{a} x}{\left[ 1 + \left( \frac{R}{a} \right)^2 - 2 \frac{R}{a} x \right]^3} \\
	\end{align*}
	%
	Sei nun $y = a x + b$:
	%
	\begin{align*}
		\int \mathrm{d}x \, y^{-3}
		&= - \frac{1}{2 a} y^{-2} \\
		\int \mathrm{d}x \, \frac{x}{y^3} = \frac{1}{a^2} \left\{ - \frac{1}{y} + \frac{b}{2 y^2} \right\}
	\end{align*}
	%
	Mit diesem Wissen:
	%
	\begin{align*}
		F &= 
		\begin{aligned}[t]
			&- \frac{q^2}{2 a^2} \frac{R}{a} \left( 1 - \left( \frac{R}{a} \right)^2 \right)
			\left\{
				\vphantom{\frac{1 - \left( \frac{R}{a} \right)^2}{2 \left[ 1 + \left( \frac{R}{a} \right)^2 - 2 \frac{R}{a} x \right]^2}}
				\frac{a}{4 R} \left[ 1 + \left( \frac{R}{a} \right)^2 - 2 \frac{R}{a} x \right]^{-2}
			\right.
			\\
			&\left.
				- \frac{a}{4 R} \left[ - \frac{1}{1 + \left( \frac{R}{a} \right)^2 - 2 \frac{R}{a} x} + \frac{1 - \left( \frac{R}{a} \right)^2}{2 \left[ 1 + \left( \frac{R}{a} \right)^2 - 2 \frac{R}{a} x \right]^2} \right]
			\right\}_{-1}^{1}
		\end{aligned} \\
		&=
		- \frac{q^2}{8 a^2} \left( 1 - \left( \frac{R}{a} \right)^2 \right)
		\left\{
			\frac{1}{1 + \left( \frac{R}{a} \right)^2 - 2 \frac{R}{a} x} + \frac{1 - \left( \frac{R}{a} \right)^2}{2 \left[ 1 - \left( \frac{R}{a} \right)^2 - 2 \frac{R}{a} x \right]^2}
		\right\}_{-1}^{1} \\
		&=
		\ldots \\
		&=
		- \frac{1}{R^2} \left( \frac{R}{a} \right)^3 \left[ 1 - \left( \frac{R}{a} \right)^2 \right]^{-2} \\
		\Aboxed{
			\bm{F} &=
			- \frac{q^2 \frac{R}{a}}{\left[ 1 - \left( \frac{R}{a} \right)^2 \right]^2} \frac{\bm{a}}{a^3}
		}
	\end{align*}
	%
	Dies ist eine attraktive Kraft auf die Ladung $q$.

	\begin{figure}[htpb]
		\centering
		\begin{pspicture}(-0.3,-3)(4,0.3)
			\psaxes[labels=none,ticks=none,arrows=->](0,0)(-0.3,-3)(4,0.3)[,0][{\color{DimGray} $\bm{F} \cdot \hat{\bm{a}}$}, 180]
			\psxTick(1){\color{DimGray} R}
			\psline[linestyle=dotted,dotsep=1pt](1,-0.7)(1,-3)
			\psplot[linecolor=MidnightBlue]{1.577}{3.8}{- (x - 1)^(-2)}
			\uput[0](1.7,-2.5){\color{MidnightBlue} $\sim (a - R)^{-2}$}
			\uput[0](3,-0.5){\color{MidnightBlue} $\sim a^{-3}$}
		\end{pspicture}
		\caption{Kraft auf eine Ladung in Abhängigkeit des Abstandes.}
	\end{figure} 
\end{example}

\begin{notice}
	\begin{enumerate}
		\item Kraft auf die Ladung $q$ durch die Spiegelladung $q'$
			%
			\begin{align*}
				\bm{F}
				&= \frac{q \cdot (-q')}{|\bm{a} - \bm{a}'|^2} \frac{\bm{a} - \bm{a}'}{|\bm{a} - \bm{a}'|} \\
				&= - \frac{q^2 \frac{R}{a}}{\left( a - \frac{R^2}{a} \right)^2} \frac{\bm{a}}{a} \\
				&= - \frac{q^2 \frac{R}{a}}{\left[ 1 - \left( \frac{R}{a} \right)^2 \right]^2} \frac{\bm{a}}{a^3}
			\end{align*}

		\item Die leitende Kugel ist geerdet, d.h.\ die Kugel ist an ein Ladungsreservoir angeschlossen. Es folgt also, dass bei Annäherung von $q$ an die Kugel deren Gesamtladung, welche auf der Oberfläche (inhomogen) konzentriert ist, von $0$ nach $-q'$ für $q$ von $\infty$ nach $a$ zunimmt.
	\end{enumerate}
\end{notice}

\begin{example} Die isolierte, leitende Kugel.

	Dieses Mal wird eine Kugel mit der Gesamtladung $Q$ betrachtet, die isoliert ist. Auch hier muss die Randbedingung $\phi |_{r = R} = \mathrm{const}$ erfüllt sein, aber die gesamte induzierte Ladung muss $0$ ergeben, da stets die Gesamtladung $Q$ vorhanden sein muss.

	\begin{notice}
		Die Gesamtladung $Q$ ist bei einer leitenden Kugel wegen der Abstoßung der Ladungsträger auch auf die Oberfläche konzentriert.
	\end{notice}

	\paragraph{Lösung:} Die Lösung setzt sich zusammen aus
	%
	\begin{enumerate}
		\item \label{itm:2013-05-31-1} Der Lösung für die geerdete, leitende Kugel. $q'$ und $a'$ sind wie dort gegeben. Dieser Beitrag liefert $0$ bei $r = R$.
			
		\item \label{itm:2013-05-31-2} Einem zusätzlichen Term einer Punktladung im Zentrum. Dieser Beitrag liefert eine Konstante bei $r = R$.
	\end{enumerate}
	%
	Betrachten wir die funktionale Form dieser Lösung:
	%
	\begin{align*}
		\phi(\bm{r})
		&=
		\underbrace{
			\frac{q}{|\bm{r} - \bm{a}|} + \frac{-q'}{|\bm{r} - \bm{a}'|}
		}_{\text{vgl.\ \ref{itm:2013-05-31-1}}}
		+
		\underbrace{
			\frac{Q - (-q')}{|\bm{r}|}
		}_{\text{vgl.\ \ref{itm:2013-05-31-2}}}
	\end{align*}
	%
	Dieses Potential löst \[ \nabla^2 \phi = - 4 \pi \delta(\bm{r} - \bm{a}) \] im Außenraum und genügt der Randbedingung $\phi|_{r = R} = \mathrm{const}$.

	Es gilt weiterhin
	%
	\begin{align*}
		\text{Gesamte induzierte Ladung}
		&= \text{Summe der Bildladungen} \\
		&= - q' + Q - (-q') = Q
	\end{align*}

	\begin{figure}[htpb]
		\centering
		\begin{pspicture}(-1.2,-1.2)(4,1.2)
			\psline(O)(4;0)
			\pscircle(O){1.2}
			\cnode*(0,0){2pt}{O}
			\cnode*[linecolor=MidnightBlue](3,0){2pt}{q1}
			\cnode*[linecolor=DarkOrange3](0.7,0){2pt}{q2}
			% Labels
			\uput[180](O){\color{DimGray} $Q + q'$}
			\uput[-90](q1){\color{MidnightBlue} $q$}
			\uput[-90](q2){\color{DarkOrange3} $-q$}
		\end{pspicture}
		\caption{Eine isolierte, leitende Kugel der Gesamtladung $Q$ und eine außerhalb ruhende Ladung $q$.}
	\end{figure} 
\end{example}
